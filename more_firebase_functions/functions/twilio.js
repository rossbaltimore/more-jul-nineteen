const twilioAccountSid = 'ACfb749e875513a487a56bc8094d8e882a';
const twilioApiKey = 'SK7d94c4fbacd693a6bebdeebeaedd6767';
const twilioApiSecret = 'OBQY9zlk49JjtCWq7wt6uiF53b06xTZ3';
const twilioAuthToken = '49242ddf47884b1966e4b92835c3e097';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const utils = require('./utils');
const AccessToken = require('twilio').jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;
const TwilioClient = require('twilio')(twilioAccountSid, twilioAuthToken);
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const urlMetadata = require('url-metadata');
const got = require('got');
const cors = require('cors')({ origin: true });


////////////////////////////////////
// URL metadata

exports.fetchMetadata = functions.https.onCall((data, context) => {

  const url = data.url;

  return urlMetadata(url)
    .then((metadata) => { // success handler
      
      return {
        title: metadata.title,
        image: metadata.image,
        description: metadata.description,
        ogImage: metadata['og:image'],
        ogImageSecure: metadata['og:image:secure_url'],
        twitterImage: metadata['twitter:image'],
        twitterImageAlt: metadata['twitter:image:alt']
      };
    });
});

////////////////////////////////////
// Image data

exports.loadImage = functions.https.onRequest((req, res) => {

  if (req.method !== 'GET') {
    console.error('Unknown method');
    return res.status(403).send('Forbidden!');
  }

  var key = req.query.key;
  if (key !== 'qecjor43r93c24jr93234rc32n409crn3240crn2340cn') {
    console.error('Unauthorised access');
    return res.status(403).send('Forbidden!');
  }

  const url = req.query.url;

  cors(req, res, () => {
    return got(url)
      .then((response) => {
        return res.status(200).send(response.rawBody);
      });
  });
});

////////////////////////////////////
// Twilio AUTH

exports.getTwilioToken = functions.https.onCall((data, context) => {

  const userId = data.userId;

  // Create an access token which we will sign and return to the client,
  // containing the grant we just created
  const token = new AccessToken(twilioAccountSid, twilioApiKey, twilioApiSecret);
  token.identity = userId;

  // Create a Video grant which enables a client to use Video 
  const videoGrant = new VideoGrant({ });

  // Add the grant to the token
  token.addGrant(videoGrant);

  const jwt = token.toJwt();

  console.log('Twilio token: ' + jwt);

  return { "token": jwt };
});

////////////////////////////////////
// Twilio SMS

const moreNumber = '+19298008916';

function logSMS(phone, text, type = 'incoming') {
  let msgLog = {
    time: utils.now(),
    text: text,
    type: type
  };
  return admin.firestore().collection("smsLog/" + phone + "/messageList").add(msgLog);
}

function logTalkSMS(phone, isOnboarding = false) {
  let msgLog = {
    phone: phone,
    time: utils.now(),
    isOnboarding: isOnboarding
  };
  return admin.firestore().collection("smsTalkLog").add(msgLog);
}

function sendSMS(to, message, media = null) {
  let msg = {
    body: message,
    from: moreNumber,
    to: to
  };
  if (media !== null && media !== undefined) {
    msg.mediaUrl = media;
  }

  return TwilioClient.messages
    .create(msg)
    .then((twilioMsg) => {
      return logSMS(to, message, 'outgoing');
    })
    .catch((error) => {
      console.error("SMS sent failed: " + error);

      // check is we need to tag as unreachable
      if (error.message.indexOf('is not a valid phone number') !== -1 ||
          error.message.indexOf('Permission to send an SMS has not been enabled') !== -1 ||
          error.message.indexOf('pair violates a blacklist rule') !== -1) {
        let update = { unreachable: true };
        return admin.firestore().doc("phone/" + to).set(update, { merge: true });
      }
    });
}

exports.sendSMS = sendSMS;

function sendActiveSMS(phone, topicId, topic) {
  let data = phone + '::' + topicId + '::' + utils.now();
  let buff = Buffer.from(data);
  let encoded = buff.toString('base64');
  const link = 'https://talk.fumble.live/#/chat/' + encoded;

  const msg = topic.activeText + link;
  return sendSMS(phone, msg);
}

function sendInactiveSMS(phone) {
  const inactive = 'Sorry but we have no active Fumbles at the moment. :(';
  return sendSMS(phone, inactive);
}

function sendJustActivatedSMS(phone, topicId, topic) {
  let data = phone + '::' + topicId + '::' + utils.now();
  let buff = Buffer.from(data);
  let encoded = buff.toString('base64');
  const link = 'https://talk.fumble.live/#/chat/' + encoded;

  const msg = topic.justActivatedText + link;
  return sendSMS(phone, msg);
}

function sendWaitSMS(phone, topic) {
  const msg = topic.waitText;
  return sendSMS(phone, msg);
}

function sendOpenedSMS(phone, topic) {
  const msg = topic.openText;
  return sendSMS(phone, msg);
}

function sendRetentionSquadSMS(phone, topicId, topic) {
  let data = phone + '::' + topicId + '::' + utils.now();
  let buff = Buffer.from(data);
  let encoded = buff.toString('base64');
  const link = 'https://talk.fumble.live/#/chat/' + encoded;

  const msg = topic.retentionSquadText + link;
  return sendSMS(phone, msg);
}

function sendWelcomeSMS(phone, topicId, topic) {
  if (topicId === 'waitingList') {
    const text = 'Hey there! I\'m Fumblepup, your automated Fumble helper (aka Fumbler\'s best friend 🐕‍🦺)\n\nThank you for signing up for early access to Fumble. I\'ll follow up with you soon! And now that we\'re getting to know each other pretty well, here\'s my contact card. Why not add me to your contacts?';
    return sendSMS(phone, text, [
      'https://storage.googleapis.com/moretest-274fe.appspot.com/web/pup_large.gif',
      'https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
  } else {
    const text = 'Hey Fumbler! I’m Fumblepup 🐕‍🦺 I don\'t have a great grasp of time, but I know it\'s super valuable and I won\'t waste any of yours, so here’s the deal:\n\nWhenever you want to talk about the latest episode of ' + topic.shortName + ', text me the word TALK. I\'ll find great people who want to talk about it with you. Wanna try it out?\n\nText TALK, and I\'ll show you. 🐾';
    // const text = 'Hello! I\'m Fumblebot 🐶, your automated helper for Fumble. I\'ll help get you set up and ready to connect with others and talk ' + topic.shortName + '!\n\nFirst, let me tell you how this whole thing works.';
    return sendSMS(phone, text, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/pup_large.gif'])
      .then(() => {
        let update = { 
          welcomeTest: utils.now(),
          welcomeName: topic.shortName
        };
        return admin.firestore().doc("phone/" + phone).set(update, { merge: true });
      });
  }
}

/*
function sendWelcomeTextResponseSMS(phone) {
  const text = 'I\'m on it!\n\nI\'ll spin up the space for you and others to talk now. I\'ll send you a link when it\'s all set!\n\nBefore I go, there\'s two more things to cover:\n\n1️⃣ Add me to your contacts with the attached card. Otherwise, some features won\'t work (and I\'ll be sad 🐶)\n\n2️⃣ You can also share the link with your friends to bring some familiar voices to the Fumble. Just remember: the links don\'t last long. After a few hours, the space (and any trace of its existence) disappears 💨\n\nHere\'s my contact card:';
  return sendSMS(phone, text)
    .then(() => {
      return sendSMS(phone, '', ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
    })
    .then(() => {
      let update = { welcomeStage: 2 };
      return admin.firestore().doc("phone/" + phone).set(update, { merge: true });
    });
}
*/

function sendWelcomeTextNoResponseSMS(phone) {
  const text = 'We know each other pretty well, so add me to your contacts. Whenever you want to talk about your favorite content, just say my favorite command (TALK) and I\'ll get a pod together. 🧙‍♂️🔮';
  // const text = 'You can text TALK whenever you feel like it. I\'ll be here (and really happy to see you 🐕‍🦺)\n\nIn the meantime, there\'s two more things to cover:\n\n1️⃣ Add me to your contacts with the attached card. Otherwise, some features won\'t work (and I\'ll be sad 🐶)\n\n2️⃣ You can also share the link with your friends to bring some familiar voices to the Fumble. Just remember: the links don\'t last long. After a few hours, the space (and any trace of its existence) disappears 💨\n\nHere\'s my contact card:';
  return sendSMS(phone, text, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
  /*
    .then(() => {
      let update = { welcomeStage: 4 };
      return admin.firestore().doc("phone/" + phone).set(update, { merge: true });
    });
  */
}

function sendSTOPSms(phone) {
  const text = 'I\'ve removed you from all further messages from me. It breaks my heart to see you 🐾.\n\nIf you want to start receiving messages again, text START.';
  return sendSMS(phone, text);
}

function sendSTARTSms(phone, topic) {
  const text = 'Welcome back! You\'ll now start receiving messages again. As a reminder, whenever you want to talk about the latest episode of ' + topic.shortName + ', just say TALK';
  return sendSMS(phone, text);
}

function sendHELPSms(phone) {
  const text = 'Here are the commands I know how to respond to:\n\n1️⃣ When you want to talk about the latest podcast episode, say TALK\n2️⃣ If you want to stop receiving messages from me, say STOP\n3️⃣ To see this list of commands again, say HELP\n4️⃣ If you need more help, you can send one of my owners an email at ross@fumble.live.';
  return sendSMS(phone, text);
}

function sendUnknownCommandSms(phone) {
  const text = 'I\'m sorry, I only know a few commands right now. You can say HELP to see a list of commands I know.';
  return sendSMS(phone, text);
}

function sendPROFILESms(phone) {
  let data = phone + '::';
  let buff = Buffer.from(data);
  let encoded = buff.toString('base64');
  const link = 'https://talk.fumble.live/#/profile/' + encoded;

  const text = 'Here is the link to edit your Fumble profile: ' + link;
  return sendSMS(phone, text);
}

function sendROOMSms(phone, roomId) {
  let data = phone + '::' + roomId + '::1';
  let buff = Buffer.from(data);
  let encoded = buff.toString('base64');
  const link = 'https://talk.fumble.live/#/room/' + encoded;

  const text = 'Here is the link to your new Fumble room: ' + link;
  return sendSMS(phone, text);
}

const interestTreshold = 2;

// incoming SMS

/*
function welcomeTest(phone) {
  let promiseStack = [];
                  
  // remove flag
  const update = { welcomeTest: admin.firestore.FieldValue.delete() };
  let promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
  promiseStack.push(promise);

  // send SMS
  promise = sendWelcomeTextResponseSMS(phone);
  promiseStack.push(promise);

  // log 
  promise = logTalkSMS(phone, true);
  promiseStack.push(promise);

  return Promise.all(promiseStack).then(() => { return null; });
}
*/

exports.sms = functions.https.onRequest((req, res) => {
  
  if (req.method !== 'POST') {
    console.log('SMS error!');
    return res.status(403).send('Forbidden!');
  }

  var phone = req.body.From;
  var msg = req.body.Body.trim();

  console.log('Incoming SMS from: ' + phone + ' --> ' + msg);

  return processSMS(phone, msg, res);
});

exports.smsTest = functions.https.onRequest((req, res) => {

  if (req.method !== 'POST') {
    console.log('SMS error!');
    return res.status(403).send('Forbidden!');
  }

  var phone = req.body.phone;
  var msg = req.body.msg.trim();

  console.log('Incoming TEST SMS from: ' + phone + ' --> ' + msg);

  return processSMS(phone, msg, res);
});

function processSMS(phone, msg, res) {

  logSMS(phone, msg);

  const uppercased = msg.toUpperCase();

  if (uppercased.startsWith('TALK')) {
    return processTALK(phone, msg, res);
  } else if (uppercased.startsWith('STOP')) {
    return processSTOP(phone, msg, res);
  } else if (uppercased.startsWith('START')) {
    return processSTART(phone, msg, res);
  } else if (uppercased.startsWith('HELP')) {
    return processHELP(phone, msg, res);
  } else if (uppercased.startsWith('PROFILE')) {
    return processPROFILE(phone, msg, res);
  } else if (uppercased.startsWith('ROOM')) {
    return processROOM(phone, msg, res);
  } else if (uppercased.startsWith('YES')) {
    return processYES(phone, msg, res);
  } else if (uppercased.startsWith('NO')) {
    return processNO(phone, msg, res);
  } else if (uppercased.startsWith('HTTPS://WWW.YOUTUBE.COM') || uppercased.startsWith('HTTP://WWW.YOUTUBE.COM') ||
             uppercased.startsWith('HTTPS://YOUTUBE.COM') || uppercased.startsWith('HTTP://YOUTUBE.COM') || 
             uppercased.startsWith('HTTPS://YOUTU.BE') || uppercased.startsWith('HTTP://YOUTU.BE')) {
    return processYoutube(phone, msg, res);
  } else if (uppercased.startsWith('HTTPS://WWW.TIKTOK.COM') || uppercased.startsWith('HTTP://WWW.TIKTOK.COM') || 
             uppercased.startsWith('HTTPS://TIKTOK.COM') || uppercased.startsWith('HTTP://TIKTOK.COM') || 
             uppercased.startsWith('HTTPS://VM.TIKTOK.COM') || uppercased.startsWith('HTTP://VM.TIKTOK.COM')) {
    return processTikTok(phone, msg, res);
  } else {
    return processUnknownCommand(phone, msg, res);
  }
}

function processTALK(phone, msg, res) {
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        // welcome test
        /*
        if (reg.welcomeTest !== undefined && reg.welcomeTest !== undefined) {
          return welcomeTest(phone).then(() => { return null; });
        }
        */

        // find topics
        let promiseStack = [];
        for (const topic in reg.topics) {
          if (reg.topics[topic] === true) {
            const promise = admin.firestore().doc("topic/" + topic).get()
            promiseStack.push(promise);
          }
        }
        return Promise.all(promiseStack);
      }
      return null;
    })
    .then((topicRefs) => {

      if (topicRefs === null || topicRefs === undefined) {
        return null;
      }

      // check if there is a topic active now
      const now = utils.now();
      for (var i = 0; i < topicRefs.length; i++) {
        const topicRef = topicRefs[i];
        if (topicRef === null || topicRef === undefined) {
          continue;
        }
        const topic = topicRef.data();

        if (topic.event === null || topic.event === undefined) {
          continue;
        }
        const event = topic.event;
        if (event.startAt <= now && event.endAt >= now) {
          return {topic: topic, id: topicRef.id};
        }
      }
      return true;
    })
    .then((topicData) => {

      if (topicData === null || topicData === undefined) {
        return null;
      }

      // send no active now
      if (topicData === true) {
        return sendInactiveSMS(phone).then(() => { return null; });
      }

      const topic = topicData.topic;
      const topicId = topicData.id;

      // topic active ?
      if (topic.event.waiting === true || topic.event.opened === true) {
        return sendActiveSMS(phone, topicId, topic).then(() => { return null; });
      }

      // topic inactive 
      let promiseStack = [];

      // flag topic is in waiting mode
      data = { 
        waiting: true,
        waitingAt: utils.now(),
        waitingPhone: phone
      };
      update = { event: data };
      promise = admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
      promiseStack.push(promise);

      // send I'm on it
      promise = sendWaitSMS(phone, topic);
      promiseStack.push(promise);

      return Promise.all(promiseStack).then(() => { return {topic: topic, id: topicId}; }); 
    })
    .then((topicData) => {
      if (topicData === null || topicData === undefined) {
        return null;
      }
      
      const topic = topicData.topic;
      const topicId = topicData.id;

      // push topicId
      let promiseStack = [];
      let promise = Promise.resolve(topicData);       
      promiseStack.push(promise);


      // get registrations
      promise = admin.firestore().collection("topic/" + topicId + '/registrations').get();
      promiseStack.push(promise);

      return Promise.all(promiseStack);
    })
    .then((data) => {
      if (data === null || data === undefined || data.length < 2) {
        return null;
      }

      const topicData = data[0];
      const topic = topicData.topic;
      const topicId = topicData.id;
      const regs = data[1];

      let promiseStack = [];
      regs.forEach((regRef) => {
        const regPhone = regRef.id;
        if (regPhone !== phone) {
          const promise = sendJustActivatedSMS(regPhone, topicId, topic);
          promiseStack.push(promise);
        }
      });

      const promise = logTalkSMS(phone);
      promiseStack.push(promise);

      return Promise.all(promiseStack);
    })
    .then(() => {
      return res.status(200).send('OK');
    });
}

function processSTOP(phone, msg, res) {

  // disable phone
  let update = { disabled: true };
  return admin.firestore()
    .doc("phone/" + phone)
    .set(update, { merge: true })
    // .then(() => {
    //   return sendSTOPSms(phone);
    // })
    .then(() => {
      return res.status(200).send('OK');
    });
}

function processSTART(phone, msg, res) {

  // enable phone
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        // welcome test
        /*
        if (reg.welcomeTest !== undefined && reg.welcomeTest !== undefined) {
          return null;
        }
        */

        // find one topic
        let topicId = null;
        for (const topic in reg.topics) {
          topicId = topic;
          break;
        }
        if (topicId !== null) {
          return admin.firestore().doc("topic/" + topicId).get()
        }
      }
      return null;
    })
    .then((topicRef) => {
      if (topicRef === null || topicRef === undefined) {
        return null;
      }

      const topic = topicRef.data();
      let promiseStack = [];

      // enable phone
      let update = { disabled: false };
      let promise = admin.firestore()
        .doc("phone/" + phone)
        .set(update, { merge: true });
      promiseStack.push(promise);

      // // send Sms
      // promise = sendSTARTSms(phone, topic);
      // promiseStack.push(promise);

      return Promise.all(promiseStack);
    })
    .then(() => {
      return res.status(200).send('OK');
    });
}

function processHELP(phone, msg, res) {
  // return sendHELPSms(phone)
  //   .then(() => {
  //     return res.status(200).send('OK');
  //   });
  return res.status(200).send('OK');
}

function processPROFILE(phone, msg, res) {

  // enable phone
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        if (reg.roomEnabled === true) {
          return sendPROFILESms(phone);
        }

        return 'unknown';
      }
      return null;
    })
    .then((result) => {
      if (result === 'unknown') {
        return sendUnknownCommandSms(phone);
      }
      return null;
    }) 
    .then(() => {
      return res.status(200).send('OK');
    });
}

function processROOM(phone, msg, res) {

  // enable phone
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        if (reg.roomEnabled === true) {
          return 'ok';
        }

        return 'unknown'
      }
      return null;
    })
    .then((result) => {
      if (result === 'unknown') {
        return sendUnknownCommandSms(phone);
      } else if (result === 'ok') {
        return admin.firestore().collection('rooms')
          .add({ owner: phone })
          .then((docRef) => {
            return sendROOMSms(phone, docRef.id);
          });
      }
      return null;
    })
    .then(() => {
      return res.status(200).send('OK');
    });
}

function youtube_parser(url) {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length===11)? match[7] : false;
}

function processYoutube(phone, msg, res) {

  // extract youtube URL
  const youtubeUrl = msg.split(' ')[0];
  const youtubeId = youtube_parser(youtubeUrl);

  // enable phone
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        if (reg.roomEnabled === true) {
          return 'ok';
        }

        return 'unknown'
      }
      return null;
    })
    .then((result) => {
      if (result === 'unknown') {
        return sendUnknownCommandSms(phone);
      } else if (result === 'ok') {
        const url = 'https://www.youtube.com/oembed?url=' + youtubeUrl + '&format=json';
        return got(url);
      }
      return null;
    })
    .then((value) => {
      if (value !== null && value.body !== undefined && value.body !==  null) {
        const youtubeData = JSON.parse(value.body);
        return {
          title: youtubeData.title,
          author_name: youtubeData.author_name,
          author_url: youtubeData.author_url,
          thumbnail: youtubeData.thumbnail_url,
          video_url: youtubeUrl
        };
      }
      return null;
    })
    .then((data) => {
      if (data !== null) {
        return admin.firestore().collection('rooms')
          .add({ 
            owner: phone,
            title: data.title,
            chapters: {
              0: {
                duration: 0,
                startAt: 0,
                title: 'Discussion'
              }
            }
          })
          .then((docRef) => {
            return admin.firestore().collection('rooms').doc(docRef.id).collection('videos')
              .add({
                type: 'youtube',
                url: youtubeId,
                state: 'stopped',
                showAt: 0,
                offset: 0
              })
              .then(() => {
                return sendROOMSms(phone, docRef.id);    
              });
          });
      }
      return null;
    })
    .then(() => {
      return res.status(200).send('OK');
    });

}

function processTikTok(phone, msg, res) {

  // extract TikTok URL
  var tikTokUrl = msg.split(' ')[0];
  // const tikTokId = tiktok_parser(youtubeUrl);

  // enable phone
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        if (reg.roomEnabled === true) {
          return 'ok';
        }

        return 'unknown'
      }
      return null;
    })
    .then((result) => {
      if (result === 'unknown') {
        return sendUnknownCommandSms(phone);
      } else if (result === 'ok') {

        var headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'};
        return got(tikTokUrl, {headers: headers})
          .then((response) => {
            tikTokUrl = response.url;
            const url = 'https://www.tiktok.com/oembed?url=' + tikTokUrl;
            return got(url);
          });
      }
      return null;
    })
    .then((value) => {
      if (value !== null && value.body !== undefined && value.body !== null) {
        const tiktokData = JSON.parse(value.body);
        return {
          title: tiktokData.title,
          author_name: tiktokData.author_name,
          author_url: tiktokData.author_url,
          thumbnail: tiktokData.thumbnail_url,
          embed: tiktokData.html,
          video_url: tikTokUrl
        };
      }
      return null;
    })
    .then((data) => {
      if (data !== null) {
        return admin.firestore().collection('rooms')
          .add({ 
            owner: phone,
            title: data.title,
            chapters: {
              0: {
                duration: 0,
                startAt: 0,
                title: 'Discussion'
              }
            }
          })
          .then((docRef) => {
            return admin.firestore().collection('rooms').doc(docRef.id).collection('shareables')
              .add({
                type: 'tiktok',
                url: data.embed,
                showAt: 0
              })
              .then(() => {
                return sendROOMSms(phone, docRef.id);    
              });
          });
      }
      return null;
    })
    .then(() => {
      return res.status(200).send('OK');
    });
}

function processUnknownCommand(phone, msg, res) {
  return admin.firestore().doc("phone/" + phone)
    .get()
    .then((regRef) => {
      if (regRef.exists) {
        const reg = regRef.data();

        if (reg.blocked === true) {
          return null;
        }

        if (reg.enableChat === true) {
          return 'ok';
        }

        return 'unknown';
      }
      return null;
    })
    .then((result) => {
      if (result === 'unknown') {
        return sendUnknownCommandSms(phone);
      }
      return null;
    })
    .then(() => {
      return res.status(200).send('OK');
    });
}

exports.dispatchSMS = functions.https.onRequest((req, res) => {
  if (req.method !== 'POST') {
    console.error('Dispatch SMS error! Non POST HTTP method');
    return res.status(403).send('Forbidden!');
  }

  var key = req.body.key;
  if (key !== 'aoisjfn23oiejffjejrfp34wejfpowe354') {
    console.error('Dispatch SMS error! Unauthorised access');
    return res.status(403).send('Forbidden!');
  }

  var phone = req.body.phone;
  var msg = req.body.msg;

  if (phone === null || phone === undefined || msg === null || msg === undefined) {
    console.error('Dispatch SMS error! Missing parameters');
    return res.status(403).send('Forbidden!');
  }

  if (phone === 'all') {
    return admin.firestore().collection("phone").get()
      .then((phones) => {
        if (phones === null || phones === undefined || phones.length === 0) return null;

        let promiseStack = [];
        phones.forEach((child) => {
          const phoneNum = child.id;
          if (child.blocked === true || child.disabled === true || child.unreachable === true) {
            return;
          }
          let promise = sendSMS(phoneNum, msg);
          promiseStack.push(promise);            
        });
        return Promise.all(promiseStack)
          .then(() => {
            return res.status(200).send('OK');
          });
      });
  } else {
    return sendSMS(phone, msg)
      .then(() => {
        return res.status(200).send('OK');
      });
  }
});

// register phone number for SMS notifications

exports.registerSms = functions.https.onRequest((req, res) => {
  
  if (req.method !== 'POST') {
    console.log('Register SMS error! Non POST HTTP method');
    return res.status(403).send('Forbidden!');
  }

  var topic = req.body.topic;
  var phone = req.body.phone;
  var data = req.body;

  if (phone === null || phone === undefined) {
    console.error('Register SMS error! Missing parameters');
    return res.status(403).send('Forbidden!');
  }

  if (topic === null || topic === undefined) {
    topic = 'test-topic';
  }

  // clean phone format 
  phone = phone.replace(/[-()\s]/g, '');
  if (!phone.startsWith('+')) {
    if (phone.startsWith('1')) {
      phone = '+' + phone;
    } else {
      phone = '+1' + phone;
    }
  }

  return admin.firestore().doc("phone/" + phone)
      .get()
      .then((regRef) => {
        if (regRef.exists) {
          const reg = regRef.data();

          // blocked or unreachable
          if (reg.blocked === true || reg.unreachable === true) {
            return null;
          }
        }
        return internalRegister(phone, topic, data, res);
      });
});

function internalRegister(phone, topicId, data, res) {

  console.log('Register SMS --> phone: ' + phone + ', topic: ' + topicId);

  return admin.firestore().doc("topic/" + topicId)
    .get()
    .then((topicRef) => {
      if (topicRef.exists) {
        const topic = topicRef.data();
        return internalRegister2(phone, topicId, topic, data, res);
      } else {
        res.status(500).send('Server Error');
        console.error("Register SMS failed: Unknown topic");
        return null;
      }
    })
    .catch((error) => {
      res.status(500).send('Server Error');
      console.error("Register SMS failed: " + error);
    });
}

function internalRegister2(phone, topicId, topic, data, res) {

  let promiseStack = [];

  // add phone to topic
  let user = { createdAt: utils.now() };
  let promise = admin.firestore().doc('topic/' + topicId + '/registrations/' + phone).set(user, { merge: true });
  promiseStack.push(promise);

  // add topic to phone
  let update = {};
  let updateData = {}
  updateData[topicId] = true;
  update["topics"] = updateData;
  if (data.firstName !== undefined && data.firstName !== null) {
    update['firstName'] = data.firstName;
  }
  if (data.lastName !== undefined && data.lastName !== null) {
    update['lastName'] = data.lastName;
  }
  if (data.email !== undefined && data.email !== null) {
    update['email'] = data.email;
  }
  promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
  promiseStack.push(promise);

  // send welcome message
  promise = sendWelcomeSMS(phone, topicId, topic);
  promiseStack.push(promise);

  return Promise.all(promiseStack)
    .then(() => {
      return res.status(200).send('OK');
    })
    .catch((error) => {
      res.status(500).send('Server Error');
      console.error("Register SMS failed: " + error);
    });
}

// triggered every time the topic web page is opened by the first user

exports.topicOpened = functions.https.onCall((data, context) => {

  const topicId = data.topicId;
  return admin.firestore().doc("topic/" + topicId).get()
    .then((ref) => {
      if (ref === null || ref === undefined) return null;

      const topicId = ref.id;
      const topic = ref.data();
      
      let promiseStack = [];

      // flag topic is opened
      let data = { 
        waiting: admin.firestore.FieldValue.delete(),
        waitingAt: admin.firestore.FieldValue.delete(),
        opened: true,
        openedAt: utils.now(),
        tick: utils.now()
      };
      let update = { event: data };
      let promise = admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
      promiseStack.push(promise);

      // send msg to waiter
      if (topic.event.waitingPhone !== null && topic.event.waitingPhone !== undefined) {
        promise = sendActiveSMS(topic.event.waitingPhone, topicId, topic);
        promiseStack.push(promise);
      }

      return Promise.all(promiseStack);
    });
});

// triggered by web apps on every tick (keep alive)

exports.topicTick = functions.https.onCall((data, context) => {

  const topicId = data.topicId;
  return admin.firestore().doc("topic/" + topicId).get()
    .then((ref) => {
      if (ref === null || ref === undefined) return null;

      const topicId = ref.id;
      const topic = ref.data();
      
      let promiseStack = [];

      // tick
      let data = {
        opened: true,
        tick: utils.now()
      };
      let update = { event: data };
      return admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
    });
});

// notify users a new topic is active
// not used for now

exports.tenmin_job = functions.pubsub
  .topic('tenmin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const tenMinsAgo = nowTime - 600;

    return admin.firestore().collection("topic")
      .where("event.startAt", ">=", tenMinsAgo)
      .where("event.startAt", "<=", nowTime)
      .get()
      .then((topics) => {

        if (topics === null || topics === undefined) return null;

        let promiseStack = [];
        topics.forEach((child) => {
          const topicId = child.id;
          const topic = child.data();
          let promise = admin.firestore().collection("topic/" + topicId + '/registrations')
            .get()
            .then((regs) => {
              if (regs === null || regs === undefined) return null;

              let promiseStack = [];
              regs.forEach((regRef) => {
                const phone = regRef.id;
                const promise = sendActiveSMS(phone, topicId, topic);
                promiseStack.push(promise);
              });
              return Promise.all(promiseStack);
            });
          promiseStack.push(promise);
        });
        return Promise.all(promiseStack);
      });
  });

// change the event time for existing topic(s)
// refresh start and end times for topic

exports.daily_job = functions.pubsub.schedule('0 13 * * *')
  .timeZone('America/New_York')
  .onRun((context) => {
  
    const topicId = 'test-topic';

    const start = new Date();
    start.setHours(14);
    start.setMinutes(0);

    const end = new Date();
    end.setDate(end.getDate() + 1);
    start.setHours(12);
    start.setMinutes(0);


    let update = {};
    update["event"] = {
      startAt: utils.dateToCocoa(start),
      endAt: utils.dateToCocoa(end),
      started: admin.firestore.FieldValue.delete(),
      waiting: admin.firestore.FieldValue.delete(),
      waitingAt: admin.firestore.FieldValue.delete(),
      waitingPhone: admin.firestore.FieldValue.delete(),
      opened: admin.firestore.FieldValue.delete(),
      openedAt: admin.firestore.FieldValue.delete(),
      tick: admin.firestore.FieldValue.delete()
    };
    return admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
});

// check for users with welcome test waiting
// if user did not respond with TALK to welcome message
// send notice

exports.fivemin_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("phone")
    .where("welcomeTest", "<", fiveMinsAgo)
    .get()
    .then((phones) => {

      if (phones === null || phones === undefined) return null;

      let promiseStack = [];
      phones.forEach((child) => {
        const phone = child.id;

        // remove flag
        const update = { 
          welcomeTest: admin.firestore.FieldValue.delete(),
          welcomeName: admin.firestore.FieldValue.delete()
        };
        let promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
        promiseStack.push(promise);

        // send SMS
        promise = sendWelcomeTextNoResponseSMS(phone);
        promiseStack.push(promise);
      });
      return Promise.all(promiseStack);
    });
  });

// check topics in waiting mode
// if topic waiting mode for more than 30 mins
// cleanup

exports.topic_waiting_cleanup_job = functions.pubsub
  .topic('tenmin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const thirtyMinsAgo = nowTime - 1800;

    return admin.firestore().collection("topic")
      .where("event.waitingAt", "<=", thirtyMinsAgo)
      .get()
      .then((topics) => {

        if (topics === null || topics === undefined) return null;

        let promiseStack = [];
        topics.forEach((child) => {
          const topicId = child.id;
          const topic = child.data();

          let data = { 
            waiting: admin.firestore.FieldValue.delete(),
            waitingAt: admin.firestore.FieldValue.delete(),
            waitingPhone: admin.firestore.FieldValue.delete(),
            opened: admin.firestore.FieldValue.delete(),
            openedAt: admin.firestore.FieldValue.delete(),
            tick: admin.firestore.FieldValue.delete()
          };
          let update = { event: data };
          let promise = admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
          promiseStack.push(promise);
        });
        return Promise.all(promiseStack);
      });
  });


// check topics in opened mode
// if topic opened but the original user did not open the link 
// send notification

exports.topic_opened_notify_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("topic")
      .where("event.openedAt", "<=", fiveMinsAgo)
      .get()
      .then((topics) => {

        if (topics === null || topics === undefined) return null;

        let promiseStack = [];
        topics.forEach((child) => {
          const topicId = child.id;
          const topic = child.data();

          let data = {
            openedAt: admin.firestore.FieldValue.delete(),
          };
          let update = { event: data };
          let promise = admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
          promiseStack.push(promise);

          // send msg to waiter
          if (topic.event.waitingPhone !== null && topic.event.waitingPhone !== undefined) {
            promise = sendOpenedSMS(topic.event.waitingPhone, topic);
            promiseStack.push(promise);
          }

        });
        return Promise.all(promiseStack);
      });
  });

// check topics not ticking 
// if topic tick older than 5 mins
// cleanup

exports.topic_tick_cleanup_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("topic")
      .where("event.opened", "=", true)
      .where("event.tick", "<=", fiveMinsAgo)
      .get()
      .then((topics) => {

        if (topics === null || topics === undefined) return null;

        let promiseStack = [];
        topics.forEach((child) => {
          const topicId = child.id;
          const topic = child.data();

          let data = { 
            waiting: admin.firestore.FieldValue.delete(),
            waitingAt: admin.firestore.FieldValue.delete(),
            waitingPhone: admin.firestore.FieldValue.delete(),
            opened: admin.firestore.FieldValue.delete(),
            openedAt: admin.firestore.FieldValue.delete(),
            tick: admin.firestore.FieldValue.delete()
          };
          let update = { event: data };
          let promise = admin.firestore().doc("topic/" + topicId).set(update, { merge: true });
          promiseStack.push(promise);
        });
        return Promise.all(promiseStack);
      });
  });

// check topics not opening 
// if topic tick not opened in less than 5 mins
// notify retention squad

exports.topic_retention_squad_notice_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("topic")
      .where("event.waitingAt", "<=", fiveMinsAgo)
      .get()
      .then((topics) => {

        if (topics === null || topics === undefined) return null;

        let promiseStack = [];

        // store topics
        let promise = Promise.resolve(topics);
        promiseStack.push(promise);

        // store retention squaders

        promise = admin.firestore().collection("phone")
          .where("retentionSquad", "=", true)
          .get()
        promiseStack.push(promise);
        return Promise.all(promiseStack);

      })
      .then((data) => {
        if (data === null || data === undefined || data.length < 2) return null;

        const topics = data[0];
        const squad = data[1];

        let promiseStack = [];

        squad.forEach((child) => {
          const phone = child.id;

          topics.forEach((child) => {
            const topicId = child.id;
            const topic = child.data();

            let promise = sendRetentionSquadSMS(phone, topicId, topic);
            promiseStack.push(promise);            
          });
        });
        return Promise.all(promiseStack);
      });
  });

// change the event time for existing topic(s)
// refresh start and end times for topic

/*
exports.welcome_msgs_job = functions.pubsub.schedule('* * * * *')
  .onRun((context) => {

    return admin.firestore().collection("phone")
    .where("welcomeStage", "<", 10)
    .get()
    .then((phones) => {

      if (phones === null || phones === undefined) return null;

      let promiseStack = [];
      phones.forEach((child) => {
        const phone = child.id;
        const stage = child.data().welcomeStage;
        const shortName = child.data().welcomeName;

        // step 1 - 2nd message in welcome flow
        if (stage === 1) {

          const text = 'Let\'s say you\'re watching the latest episode of ' + shortName + '. They\'re having a REALLY stimulating discussion. You want to talk about the topic they\'re talking about. What do you do?\n\n✅ Text TALK to this number. Then, I\'ll swoop into action 🤹‍♀️ and do some behind-the-scenes magic. Within a few minutes, I\'ll send you a link to a live "space" with other people who want to talk, too.\n\nBtw, a "space" is an audio room where fumblers can talk about the latest ' + shortName + ' episode right now.\n\nWant to try it out? Text the word TALK now.';
          let promise = sendSMS(phone, text);
          promiseStack.push(promise);

          // wait for TALK reponse
          const update = { 
            welcomeStage: admin.firestore.FieldValue.delete(),
            welcomeTest: utils.now()
          };
          promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
          promiseStack.push(promise);

        // step 2 - 2nd mesasge in welcome TALK response flow
        } else if (stage === 2) {

          let data = phone + '::welcome::' + utils.now();
          let buff = Buffer.from(data);
          let encoded = buff.toString('base64');
          const link = 'https://talk.fumble.live/#/chat/' + encoded;
      
          const text = 'The space is ready! Here\'s the link so you can check it out: ' + link;
          let promise = sendSMS(phone, text);
          promiseStack.push(promise);

          // step 3
          const update = { welcomeStage: 3 };
          promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
          promiseStack.push(promise);

        // step 3 - 3rd message in welcome TALK response flow
        } else if (stage === 3) {
          const text = 'From now on, whenever I send you a link like that one, you\'ll be able to connect with others and talk ' + shortName + '. This time, because we\'re just trying it out, there\'s no one there yet.\n\nThat\'s all for now! Remember, whenever you\'re watching ' + shortName + ' and want to talk, text me the word TALK and I\'ll spin up a space for you and find others to join you right then and there.';
          let promise = sendSMS(phone, text);
          promiseStack.push(promise);

          // we're done
          const update = { 
            welcomeStage: admin.firestore.FieldValue.delete(),
            welcomeName: admin.firestore.FieldValue.delete()
          };
          promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
          promiseStack.push(promise);

        // step 4 - 2nd message in welcome no TALK response flow
        } else if (stage === 4) {
          const text = 'That\'s all for now! Remember, whenever you\'re watching ' + shortName + ' and want to talk, text me the word TALK and I\'ll spin up a space for you and find others to join you right then and there.';
          let promise = sendSMS(phone, text);
          promiseStack.push(promise);

          // we're done
          const update = { 
            welcomeStage: admin.firestore.FieldValue.delete(),
            welcomeName: admin.firestore.FieldValue.delete()
          };
          promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
          promiseStack.push(promise);

        // some error
        } else {

          // we're done
          const update = { 
            welcomeStage: admin.firestore.FieldValue.delete(),
            welcomeName: admin.firestore.FieldValue.delete()
          };
          let promise = admin.firestore().doc("phone/" + phone).set(update, { merge: true });
          promiseStack.push(promise);
        }
      });
      return Promise.all(promiseStack);
    });
});
*/

// triggers for block, disable, unreachable
// need to remove / bring back the subscriptions
exports.phoneUpdate = functions.firestore
  .document('phone/{phoneId}')
  .onUpdate((change, context) => {

    const phoneNum = context.params.phoneId;
    const phoneOld = change.before.data();
    const phoneNew = change.after.data();

    if (phoneOld === null) {
      return null;
    }

    const removeOld = phoneOld.blocked === true || phoneOld.disabled === true || phoneOld.unreachable === true;
    const removeNew = phoneNew.blocked === true || phoneNew.disabled === true || phoneNew.unreachable === true;

    let promiseStack = [];

    if (removeOld !== removeNew) {
      if (removeNew) {
        // if blocked / disabled / unreachable -> remove registrations
        if (phoneNew.topics !== null && phoneNew.topics !== undefined) {
          for (let topicId in phoneNew.topics) {
            if (phoneNew.topics[topicId] === true) {
              let promise = admin.firestore().doc("topic/" + topicId + "/registrations/" + phoneNum).delete();
              promiseStack.push(promise);
            }
          }
        }
      } else {
        // if un blocked / disabled / unreachable -> add registrations
        if (phoneNew.topics !== null && phoneNew.topics !== undefined) {
          for (let topicId in phoneNew.topics) {
            if (phoneNew.topics[topicId] === true) {
              let promise = admin.firestore().doc("topic/" + topicId + "/registrations/" + phoneNum).set({'createdAt': utils.now()});
              promiseStack.push(promise);
            }
          }
        }
      }
    }

    return Promise.all(promiseStack);
  });


////////////////////////////////////
// registration for room peeps

// after leaving the room
exports.room_left_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("phoneUsers")
      .where("tickAt", "<=", fiveMinsAgo)
      .get()
      .then((phoneUsers) => {

        if (phoneUsers === null || phoneUsers === undefined) return null;

        let promiseStack = [];

        phoneUsers.forEach((child) => {
          const userId = child.id;
          const user = child.data();

          let update = {};

          if (user.signUpDone !== true && user.phoneNumber !== null && user.phoneNumber !== undefined)  {
            const msg = 'Hey there! I\'m Fumblepup, your automated Fumble helper (aka Fumbler\'s best friend 🐕‍🦺)\n\nI hope you enjoyed your first experience! I can let you know whenever ' + user.signUpHost + ' goes live again. If you want that, just say YES now.';
            let promise = sendSMS(user.phoneNumber, msg, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/pup_large.gif']);
            promiseStack.push(promise);
            update['signUpDone'] = true;
            update['signUpTime'] = utils.now();
            update['signUpNextDayAt'] = utils.now();
          }

          update['tickAt'] = admin.firestore.FieldValue.delete();
          let promise = admin.firestore().collection("phoneUsers").doc(userId).set(update, { merge: true });
          promiseStack.push(promise);            
        });

        return Promise.all(promiseStack);
      });
  });

// if the user did not respond to post leave msg
exports.room_left_msgs_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    const nowTime = utils.now();
    const fiveMinsAgo = nowTime - 300;

    return admin.firestore().collection("phoneUsers")
      .where("signUpTime", "<=", fiveMinsAgo)
      .get()
      .then((phoneUsers) => {

        if (phoneUsers === null || phoneUsers === undefined) return null;

        let promiseStack = [];

        phoneUsers.forEach((child) => {
          const userId = child.id;
          const user = child.data();

          const msg = 'How about this: here\'s my contact card. Add me to your contacts, then when you decide, you can get back to me with a simple YES or NO answer 🐾🐾';
          let promise = sendSMS(user.phoneNumber, msg, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
          promiseStack.push(promise);

          let update = {};
          update['signUpTime'] = admin.firestore.FieldValue.delete();
        
          promise = admin.firestore().collection("phoneUsers").doc(userId).set(update, { merge: true });
          promiseStack.push(promise);
        });

        return Promise.all(promiseStack);
      });
  });

exports.room_left_next_day_job = functions.pubsub.schedule('12 19 * * *')
  .onRun((context) => {

    const nowTime = utils.now();
    const yesterday = nowTime - 69120;

    return admin.firestore().collection("phoneUsers")
      .where("signUpNextDayAt", "<=", yesterday)
      .get()
      .then((phoneUsers) => {

        if (phoneUsers === null || phoneUsers === undefined) return null;

        let promiseStack = [];

        phoneUsers.forEach((child) => {
          const userId = child.id;
          const user = child.data();

          const msg = 'Want to be able to create your own rooms like the one you were in yesterday? We\'re currently in an invite-only, private beta period, but you can apply for an invite here: https://www.fumble.live/create';
          let promise = sendSMS(user.phoneNumber, msg);
          promiseStack.push(promise);

          let update = {};
          update['signUpNextDayAt'] = admin.firestore.FieldValue.delete();
          promise = admin.firestore().collection("phoneUsers").doc(userId).set(update, { merge: true });
          promiseStack.push(promise);
        });

        return Promise.all(promiseStack);
      }); 
  });

function processYES(phone, msg, res) {

  // check if this is room member
  return admin.firestore().collection("phoneUsers")
    .where("phoneNumber", "=", phone)
    .get()
    .then((phoneUsers) => {
      if (phoneUsers === null || phoneUsers === undefined || phoneUsers.length < 1) return false;

      let promiseStack = [];

      let commandOk = false;

      phoneUsers.forEach((child) => {
        const userId = child.id;
        const user = child.data();

        if (user.signUpHostId !== null && user.signUpHostId !== undefined) {
          let promise =admin.firestore().collection("phoneUsers")
            .doc(user.signUpHostId)
            .collection('followers')
            .doc(userId)
            .set({ phoneNumber: phone, createdAt: utils.now() }, { merge: true });    
          promiseStack.push(promise);
          commandOk = true;
        }

        let update = {};
        update['signUpHostId'] = admin.firestore.FieldValue.delete();
        update['signUpHost'] = admin.firestore.FieldValue.delete();
      
        let promise = admin.firestore().collection("phoneUsers").doc(userId).set(update, { merge: true });
        promiseStack.push(promise);            
      });

      if (commandOk) {
        const msg = 'You got it! And now that we\'re getting to know each other pretty well, here\'s my contact card. Why not add me to your contacts?';
        let promise = sendSMS(phone, msg, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
        promiseStack.push(promise);
      } else {
        return false;
      }

      return Promise.all(promiseStack);
    })
    .then((result) => {
      if (result === false) {
        return processUnknownCommand(phone, msg, res);
      }
      return res.status(200).send('OK');
    });
}

function processNO(phone, msg, res) {

  // check if this is room member
  return admin.firestore().collection("phoneUsers")
    .where("phoneNumber", "=", phone)
    .get()
    .then((phoneUsers) => {
      if (phoneUsers === null || phoneUsers === undefined || phoneUsers.length < 1) return false;

      let promiseStack = [];

      let commandOk = false;

      phoneUsers.forEach((child) => {
        const userId = child.id;
        const user = child.data();

        if (user.signUpHostId !== null && user.signUpHostId !== undefined) {
          commandOk = true;
        }

        let update = {};
        update['signUpHostId'] = admin.firestore.FieldValue.delete();
        update['signUpHost'] = admin.firestore.FieldValue.delete();
      
        let promise = admin.firestore().collection("phoneUsers").doc(userId).set(update, { merge: true });
        promiseStack.push(promise);            
      });

      if (commandOk) {
        const msg = 'No problem 🥺😢😭\n\nHow about this: here\'s my contact card. Add me to your contacts, then if you change your mind, you can let me know 🐾🐾';
        let promise = sendSMS(phone, msg, ['https://storage.googleapis.com/moretest-274fe.appspot.com/web/Fumble.vcf']);
        promiseStack.push(promise);
      } else {
        return false;
      }

      return Promise.all(promiseStack);
    })
    .then((result) => {
      if (result === false) {
        return processUnknownCommand(phone, msg, res);
      }
      return res.status(200).send('OK');
    });
}

// notify followers
exports.roomUpdate = functions.firestore
  .document('rooms/{roomId}')
  .onUpdate((change, context) => {

    const roomId = context.params.roomId;
    const roomOld = change.before.data();
    const roomNew = change.after.data();

    if (roomOld === null) {
      return null;
    }

    if (roomNew.host === null || roomNew.host === undefined) {
      return null;
    }

    let oldChapters = [];
    if (roomOld.chapters !== null && roomOld.chapters !== undefined) {
      oldChapters = roomOld.chapters;
    }
    let newChapters = [];
    if (roomNew.chapters !== null && roomNew.chapters !== undefined) {
      newChapters = roomNew.chapters;
    }

    if (oldChapters.length !== 0 || newChapters.length === 0) {
      return null;
    }
    
    let promiseStack = [];

    // followers + waiting List
    return admin.firestore()
      .collection("phoneUsers")
      .doc(roomNew.host.id)
      .collection('followers')
      .get()
      .then((followers) => {
        if (followers === null || followers === undefined) return false;

        let promiseStack = [];
        followers.forEach((child) => {
          const phone = child.data().phoneNumber;
          if (phone !== null && phone !== undefined) {
            const data = phone + '::' + roomId + '::1';
            const buff = Buffer.from(data);
            const encoded = buff.toString('base64');
            const link = 'https://talk.fumble.live/#/room/' + encoded;
            const msg = roomNew.host.name + ' is starting a new Fumble - ' + roomNew.title + '! Use this link to join: ' + link;
            const promise = sendSMS(phone, msg);
            promiseStack.push(promise);
          }
        });

        return Promise.all(promiseStack);
      })
      .then(() => {
        return admin.firestore()
          .collection("rooms")
          .doc(roomId)
          .collection('waitingList')
          .get();
      })
      .then((waitingList) => {
        if (waitingList === null || waitingList === undefined) return false;

        let promiseStack = [];
        waitingList.forEach((child) => {
          const phone = child.id;
          const data = phone + '::' + roomId + '::1';
          const buff = Buffer.from(data);
          const encoded = buff.toString('base64');
          const link = 'https://talk.fumble.live/#/room/' + encoded;
          const msg = 'Fumble is starting! Use this link to join: ' + link;
          const promise = sendSMS(phone, msg);
          promiseStack.push(promise);
        });

        return Promise.all(promiseStack);
      });
  });

// track followers
exports.phoneUserFollowerCreate = functions.firestore
  .document('phoneUsers/{userId}/followers/{followerId}')
  .onCreate((change, context) => {

    const userId = context.params.userId;
    const followerId = context.params.followerId;

    const experience = change.data();
    return admin.firestore()
      .collection("phoneUsers")
      .doc(followerId)
      .collection('following')
      .doc(userId)
      .set({ createdAt: utils.now() }, { merge: true })
      .then(() => {
        return admin.firestore()
          .collection("phoneUsers")
          .doc(userId)
          .set({ "numOfFollowers": admin.firestore.FieldValue.increment(1) }, { merge: true });
      })
      .then(() => {
        return admin.firestore()
          .collection("phoneUsers")
          .doc(followerId)
          .set({ "numOfFollowings": admin.firestore.FieldValue.increment(1) }, { merge: true });
      });
  });

exports.phoneUserFollowerDelete = functions.firestore
  .document('phoneUsers/{userId}/followers/{followerId}')
  .onDelete((change, context) => {

    const userId = context.params.userId;
    const followerId = context.params.followerId;

    const experience = change.data();
    return admin.firestore()
      .collection("phoneUsers")
      .doc(followerId)
      .collection('following')
      .doc(userId)
      .delete()
      .then(() => {
        return admin.firestore()
          .collection("phoneUsers")
          .doc(userId)
          .set({ "numOfFollowers": admin.firestore.FieldValue.increment(-1) }, { merge: true });
      })
      .then(() => {
        return admin.firestore()
          .collection("phoneUsers")
          .doc(followerId)
          .set({ "numOfFollowings": admin.firestore.FieldValue.increment(-1) }, { merge: true });
      });
  });

// track nps
exports.roomNpsCreate = functions.firestore
  .document('rooms/{roomId}/nps/{npsId}')
  .onCreate((change, context) => {

    const roomId = context.params.roomId;
    const nps = change.data();

    if (nps.score === null || nps.score === undefined) {
      return null;
    }

    let update = {};
    update['npsScore'] = admin.firestore.FieldValue.increment(nps.score);
    update['npsCount'] = admin.firestore.FieldValue.increment(1);

    return admin.firestore()
      .collection("rooms")
      .doc(roomId)
      .set(update, { merge: true });
  });

exports.roomNpsDelete = functions.firestore
  .document('rooms/{roomId}/nps/{npsId}')
  .onDelete((change, context) => {

    const roomId = context.params.roomId;
    const nps = change.data();

    if (nps.score === null || nps.score === undefined) {
      return null;
    }

    let update = {};
    update['npsScore'] = admin.firestore.FieldValue.increment(-nps.score);
    update['npsCount'] = admin.firestore.FieldValue.increment(-1);

    return admin.firestore()
      .collection("rooms")
      .doc(roomId)
      .set(update, { merge: true });
  });
