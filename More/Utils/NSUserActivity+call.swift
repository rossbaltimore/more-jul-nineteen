//
//  NSUserActivity+call.swift
//  More
//
//  Created by Luko Gjenero on 30/07/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import Foundation
import Intents

extension NSUserActivity: StartCallConvertible {

    var startCallHandle: String? {
        guard let startCallIntent = interaction?.intent,
            let handle = startCallIntent.identifier
            else {
                return nil
        }
        return handle
    }
    
    var startCallName: String? {
        if let startCallIntent = interaction?.intent as? INStartCallIntent,
            let name = startCallIntent.contacts?.first?.displayName {
                return name
        }
        if let startCallIntent = interaction?.intent as? INStartAudioCallIntent,
            let name = startCallIntent.contacts?.first?.displayName {
                return name
        }
        if let startCallIntent = interaction?.intent as? INStartVideoCallIntent,
            let name = startCallIntent.contacts?.first?.displayName {
                return name
        }
        return nil
    }
    
    var startCallIdentifier: String? {
        if let startCallIntent = interaction?.intent as? INStartCallIntent,
            let name = startCallIntent.contacts?.first?.customIdentifier {
                return name
        }
        if let startCallIntent = interaction?.intent as? INStartAudioCallIntent,
            let name = startCallIntent.contacts?.first?.customIdentifier {
                return name
        }
        if let startCallIntent = interaction?.intent as? INStartVideoCallIntent,
            let name = startCallIntent.contacts?.first?.customIdentifier {
                return name
        }
        return nil
    }

    var isVideo: Bool? {
        guard let startCallIntent = interaction?.intent as? INStartCallIntent else { return false }
        return startCallIntent.callCapability == .videoCall
    }

}


protocol StartCallConvertible {

    var startCallHandle: String? { get }
    var isVideo: Bool? { get }

}

extension StartCallConvertible {

    var isVideo: Bool? {
        return nil
    }

}
