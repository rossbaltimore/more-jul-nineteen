//
//  ExploreViewController.swift
//  More
//
//  Created by Luko Gjenero on 10/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit
import Firebase

class ExploreViewController: MoreBaseViewController {
    
    fileprivate weak var fullScreen: FullScreenExploreViewController!
    fileprivate weak var cardView: CardExploreViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        
        let fullScreen = FullScreenExploreViewController()
        fullScreen.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(fullScreen)
        view.addSubview(fullScreen.view)
        fullScreen.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        fullScreen.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        fullScreen.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        fullScreen.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        fullScreen.didMove(toParent: self)
        self.fullScreen = fullScreen
        
        let cardView = CardExploreViewController()
        cardView.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(cardView)
        view.addSubview(cardView.view)
        cardView.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        cardView.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        cardView.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        cardView.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        cardView.didMove(toParent: self)
        cardView.view.isHidden = true
        self.cardView = cardView
        
        cardView.requested = { [weak self] experience in
            self?.segmentView?.item = .now
            self?.showFullScreen(scrollToTop: true)
            self?.requested(for: experience)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(experiencesLoaded(_:)), name: ExperienceService.Notifications.ExperiencesLoaded, object: nil)
        
        // setup the deeplinking
        DeepLinkService.shared.rootView = navigationController as? MoreTabBarNestedNavigationController
        
        // mock
        // mockNewSignals()
    }
    
    deinit {
        // release the deeplinking
        DeepLinkService.shared.rootView = nil
    }
    
    private var isFirst: Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
        //
        //            if let ex = self?.dataSource.experiences.first {
        //                self?.presentJoinedVirtualBubble(for: ex)
        //            }
        //
        //        }
        
        guard isFirst else { return }
        isFirst = false
        
        firstTabSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let nc = navigationController as? MoreTabBarNestedNavigationController {
            nc.hideNavigation(hide: false, animated: false)
        }
        
        guard isFirst else { return }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let nc = navigationController {
            TutorialService.shared.hide(in: nc.view)
        }
    }
    
    @objc private func toForeground() {
        // nop
    }
    
    @objc private func experiencesLoaded(_ notice: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.firstTabSetting()
        }
    }
    
    private func firstTabSetting() {
        let experiences = ExperienceService.shared.getExperiences()
        if experiences.count > 0 {
            if experiences.first(where: { $0.activePost() != nil }) != nil {
                guard segmentView?.item == .ideas else { return }
                segmentView?.item = .now
                showFullScreen()
            } else {
                guard segmentView?.item == .now else { return }
                segmentView?.item = .ideas
                showCardView()
            }
        }
    }
    
    // MARK: - tabs
    
    func showFullScreen(scrollToTop: Bool = false) {
        
        if scrollToTop {
            fullScreen.scrollToTop(animated: false)
        }
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        view.layer.add(transition, forKey: kCATransition)
        fullScreen.view.isHidden = false
        cardView.view.isHidden = true
        fullScreen.isVisible()
    }
    
    func showCardView() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromRight
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        // navigationController?.view.layer.add(transition, forKey: kCATransition)
        view.layer.add(transition, forKey: kCATransition)
        fullScreen.view.isHidden = true
        cardView.view.isHidden = false
        cardView.isVisible()
    }
    
    // MARK: - show signal
    
    func showExperience(experience: Experience, check: Bool = false, scroll: Bool = false, showChat: Bool = false, showCall: Bool = false) {
        if experience.activePost() == nil {
            if segmentView?.item == .now {
                segmentView?.item = .ideas
                showCardView()
            }
            cardView.showExperience(experience: experience, check: check, scroll: scroll)
        } else {
            if segmentView?.item == .ideas {
                segmentView?.item = .now
                showFullScreen()
            }
            fullScreen.showExperience(experience: experience, check: check, scroll: scroll, showChat: showChat, showCall: showCall)
        }
    }
    
    func isShowingChat(chatId: String) -> Bool {
        if !fullScreen.view.isHidden {
            return fullScreen.isShowingChat(chatId: chatId)
        }
        return false
    }
    
    func isShowingNow() -> Bool {
        return !fullScreen.view.isHidden
    }
    
    // MARK: - tutorials
    /*
    private func runTutorials(cell: FullScreenExploreCollectionViewCell) {
        guard navigationController?.topViewController == self,
            presentedViewController == nil,
            navigationController?.presentedViewController == nil
            else { return }
        
        
        // check the tutorials
        if let nc = navigationController {
            if TutorialService.shared.shouldShow(tutorial: .firstCard) {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: 120), to: nc.view)
                TutorialService.shared.show(tutorial: .firstCard, anchor: anchor, container: nc.view, above: true)
            } else if TutorialService.shared.shouldShow(tutorial: .secondCard) {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: 120), to: nc.view)
                TutorialService.shared.show(tutorial: .secondCard, anchor: anchor, container: nc.view, above: true)
            } else if TutorialService.shared.shouldShow(tutorial: .timer),
                let experience = ExperienceTrackingService.shared.updatedExperienceData(for: cell.experience?.id ?? "xx"),
                experience.activePost()?.creator.isMe() == true {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: 120), to: nc.view)
                TutorialService.shared.show(tutorial: .timer, anchor: anchor, container: nc.view, above: true)
            } else if TutorialService.shared.shouldShow(tutorial: .people),
                let experience = ExperienceTrackingService.shared.updatedExperienceData(for: cell.experience?.id ?? "xx"),
                let post = experience.activePost(), !post.creator.isMe() {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: 20), to: nc.view)
                TutorialService.shared.show(tutorial: .people, anchor: anchor, container: nc.view, above: false)
            } else if TutorialService.shared.shouldShow(tutorial: .active),
                let experience = ExperienceTrackingService.shared.updatedExperienceData(for: cell.experience?.id ?? "xx"),
                experience.activePost() == nil {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: 20), to: nc.view)
                TutorialService.shared.show(tutorial: .active, anchor: anchor, container: nc.view, above: false)
            }
        }
    }
    */
}

extension ExploreViewController: UIAdaptivePresentationControllerDelegate {
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        if let vc = presentationController.presentedViewController as? CreateSignalViewController {
            vc.save()
        }
    }
}


extension MoreTabBarNestedNavigationController: ExperienceDetailsPresenter {
    
    var signalCell: ExploreCollectionViewCell? {
        let explore = topViewController as? ExploreViewController
        return explore?.cardView.signalCell
    }
    
    var topSignalBar: ExploreTopBar? {
        let explore = topViewController as? ExploreViewController
        return explore?.cardView.topSignalBar
    }
    
    var topBarSnapshot: UIImage? {
        let explore = topViewController as? ExploreViewController
        return explore?.cardView.topBarSnapshot
    }
    
    var bottomPadding: CGFloat {
        let explore = topViewController as? ExploreViewController
        return explore?.cardView.bottomPadding ?? 0
    }
    
    func pause() {
        let explore = topViewController as? ExploreViewController
        explore?.cardView.pause()
    }
       
    func start() {
        let explore = topViewController as? ExploreViewController
        explore?.cardView.start()
    }
}

// MARK: - PN tests

extension ExploreViewController {
    
    func test() {
        
        // testNewPostNotification()
        
        // testNewLikeNotification()
    }
    
    private func testNewPostNotification() {
        let content = UNMutableNotificationContent()
        content.title = "New post"
        content.body = "Testing ..."
        content.userInfo["type"] = "newPost"
        content.userInfo["experienceId"] = "q8Ob7jY4yLSNbRzjSOHh"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString,
                                            content: content, trigger: trigger)
        
        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                // Handle any errors.
            }
        }
    }
    
    private func testNewLikeNotification() {
        let content = UNMutableNotificationContent()
        content.title = "New like"
        content.body = "Testing ..."
        content.userInfo["type"] = "newLike"
        content.userInfo["experienceId"] = "VPE73nFHf60EXr7aw6De"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString,
                                            content: content, trigger: trigger)
        
        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                // Handle any errors.
            }
        }
    }
}

