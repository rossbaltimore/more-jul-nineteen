//
//  ExploreSegmentView.swift
//  More
//
//  Created by Luko Gjenero on 10/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class ExploreSegmentView: LoadableView {

    enum Item {
        case now, ideas
    }
    
    @IBOutlet private weak var nowButton: UIButton!
    @IBOutlet private weak var nowBlur: UIVisualEffectView!
    @IBOutlet private weak var ideasButton: UIButton!
    @IBOutlet private weak var ideasBlur: UIVisualEffectView!
    
    var selected: ((_ item: Item)->())?
    
    var item: Item = .now {
        didSet {
            setupUI()
        }
    }
    
    override func setupNib() {
        super.setupNib()
        item = .now
        
        nowBlur.layer.cornerRadius = 15
        nowBlur.layer.masksToBounds = true
        ideasBlur.layer.cornerRadius = 15
        ideasBlur.layer.masksToBounds = true
    }
    
    private func setupUI() {
        switch item {
        case .now:
            nowBlur.isHidden = false
            ideasBlur.isHidden = true
            nowButton.isSelected = false
            ideasButton.isSelected = false
        case .ideas:
            nowBlur.isHidden = true
            ideasBlur.isHidden = false
            nowButton.isSelected = true
            ideasButton.isSelected = true
        }
    }
    
    @IBAction private func nowTouch() {
        guard item != .now else { return }
        
        item = .now
        selected?(item)
    }
    
    @IBAction private func ideasTouch() {
        guard item != .ideas else { return }
        
        item = .ideas
        selected?(item)
    }
}
