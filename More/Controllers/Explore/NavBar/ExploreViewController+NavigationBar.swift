//
//  ExploreViewController+NavigationBar.swift
//  More
//
//  Created by Luko Gjenero on 29/09/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit
import SwiftMessages

extension ExploreViewController {

    func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage.onePixelImage(color: .clear), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage.onePixelImage(color: .clear)
        
        // left button - profile
        trackProfile()
        setLeftNavigationButton()
        
        // right button - inbox
        trackRequests()
        highlightRequestsTabIfNeeded()
        
        let titleView = ExploreSegmentView()
        titleView.selected = { [weak self] item in
            switch item {
            case .now:
                self?.showFullScreen()
            case .ideas:
                self?.showCardView()
            }
        }
        setTitle(titleView)
    }
    
    var segmentView: ExploreSegmentView? {
        return navigationItem.titleView as? ExploreSegmentView
    }
    
    // MARK: - profile button & tracking profile changes
    
    @objc private func setLeftNavigationButton() {
        let profile = AvatarImage(frame: .zero)
        profile.isUserInteractionEnabled = false
        profile.translatesAutoresizingMaskIntoConstraints = false
        profile.heightAnchor.constraint(equalToConstant: 30).isActive = true
        profile.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profile.ringSize = 3
        profile.ringColor = .clear
        profile.backgroundColor = .clear
        if let model = ProfileService.shared.userModel() {
            profile.sd_progressive_setImage(with: URL(string: model.avatarUrl))
        }
        setLeftContent(profile)
        leftTap = { [weak self] in
            self?.presentProfile()
        }
    }
    
    private func trackProfile() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(setLeftNavigationButton),
            name: ProfileService.Notifications.ProfilePhotos,
            object: nil)
    }
    
    private func presentProfile() {
        let vc = OwnProfileViewController()
        _ = vc.view
        vc.backTap = { [weak self] in
            // self?.navigationController?.popViewController(animated: true)
            self?.navigationController?.popViewController(animated: true, subType: .fromRight)
        }
        // navigationController?.pushViewController(vc, animated: true)
        navigationController?.pushViewController(vc, animated: true, subType: .fromLeft)
    }
    
    // MARK: - right button & tracking requests (chats)
    
    @objc private func setRightNavigationButton(_ unread: Int) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = false
        
        let icon = UIImageView(image: UIImage(named: "requests-inbox"))
        icon.contentMode = .scaleAspectFit
        icon.frame = CGRect(x: 7, y: 10, width: 26, height: 20)
        view.addSubview(icon)
        
        if unread > 0 {
            let badge = UILabel(frame: CGRect(x: 19, y: -4, width: 25, height: 25))
            badge.backgroundColor = .brightSkyBlue
            badge.font = UIFont(name: "DIN-Bold", size: 15)
            badge.textColor = .white
            badge.textAlignment = .center
            badge.layer.cornerRadius = 12.5
            badge.layer.masksToBounds = true
            if unread > 9 {
                badge.text = "9+"
            } else {
                badge.text = "\(unread)"
            }
            view.addSubview(badge)
        }
        
        setRightContent(view)
        rightTap = { [weak self] in
            self?.presentRequests()
        }
    }
    
    private func trackRequests() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadChats(_:)), name: ChatService.Notifications.ChatsLoaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newChatMessage(_:)), name: ChatService.Notifications.ChatMessage, object: nil)
        highlightRequestsTabIfNeeded()
    }
    
    @objc private func reloadChats(_ notice: Notification) {
        highlightRequestsTabIfNeeded()
    }
    
    @objc private func newChatMessage(_ notice: Notification) {
        highlightRequestsTabIfNeeded()
        DispatchQueue.main.async { [weak self] in
            self?.internalNewChatMessage(notice)
        }
    }
    
    private func internalNewChatMessage(_ notice: Notification) {
        guard !(presentedViewController is EnRouteV2ViewController) else { return }
        guard let chatId = notice.userInfo?["chatId"] as? String else { return }
        guard let message = notice.userInfo?["message"] as? Message else { return }
        
        // do not show if user is looking at the chat on explore
        if let nc = navigationController as? MoreTabBarNestedNavigationController,
            let top = nc.topViewController as? ExploreViewController,
            top.isShowingChat(chatId: chatId),
            message.type != .startCall {
            return
        }
        
        // do not show for opened chat
        if let nc = navigationController as? MoreTabBarNestedNavigationController,
            let chatVC = nc.topViewController as? ChatViewController,
            chatVC.chatId == chatId && !chatVC.callExpanded {
            return
        }
        
        if let type = notice.userInfo?["update"] as? String , type == "new" {
            processNeMessage(chatId: chatId, message: message)
        }
    }
        
    func processNeMessage(chatId: String, message: Message) {
        if !message.isMine(),
            !message.haveRead() && !message.wasDelivered(),
            (message.type == .text || message.type == .photo || message.type == .video ||
                message.type == .startCall || message.type == .joined || message.type == .created) {
            
            guard let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            if chat.members.first(where: { $0.isMe() }) == nil {
                let all = ExperienceTrackingService.shared.getTrackedExperiences()
                if all.first(where: { $0.activePost()?.chat?.id == chat.id }) != nil {
                    if message.type == .startCall && message.createdAt.timeIntervalSinceNow > -5 {
                        callStarted(chat: chat, message: message)
                    }
                }
                return
            }
            
            let experience = ExperienceTrackingService.shared.getActiveExperiences().first(where: { experience in
                if experience.myPost()?.chat?.id == chatId {
                    return true
                }
                if let request = experience.myRequest(), experience.post(for: request.id)?.chat?.id == chatId {
                    return true
                }
                return false
            })
            
            SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: .normal)
            let view: AlertMessageView = try! SwiftMessages.viewFromNib()
            view.configureTheme(backgroundColor: UIColor(red: 244, green: 244, blue: 244), foregroundColor: .clear)
            view.button?.backgroundColor = .clear
            
            if let post = experience?.myPost() {
                view.content.setupForMessgae(message, in: post)
            } else if let request = experience?.myRequest(), let post = experience?.post(for: request.id) {
                view.content.setupForMessgae(message, in: post)
            } else {
                view.content.setupForMessgae(message)
            }
            
            view.tapHandler = { [weak self] _ in
                self?.dismiss(animated: false, completion: nil)
                self?.showChat(chatId: chat.id, showChat: true)
            }
            
            SwiftMessages.show(view: view)
        }
        highlightRequestsTabIfNeeded()
    }
    
    private func callStarted(chat: Chat, message: Message) {
        guard !(presentedViewController is EnRouteV2ViewController) else { return }
        
        SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: .normal)
        let view: AlertCallView = try! SwiftMessages.viewFromNib()
        view.configureTheme(
            backgroundColor: .clear,
            foregroundColor: .clear)
        view.button?.backgroundColor = .clear
        
        view.setup(
            title: chat.title ?? message.sender.name,
            subtitle: "\(message.sender.name) just started a call.",
            avatar: message.sender.avatar)
        
        view.tapHandler = { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
            self?.showChat(chatId: chat.id, showCall: true)
        }
        
        var config = SwiftMessages.defaultConfig
        config.duration = .seconds(seconds: 7)
        SwiftMessages.show(config: config, view: view)
    }
    
    func showChat(chatId: String, showChat: Bool = false, showCall: Bool = false) {
        if let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }),
            let nc = navigationController as? MoreTabBarNestedNavigationController {
            
            let all = ExperienceTrackingService.shared.getTrackedExperiences()
            if let experience = all.first(where: { $0.activePost()?.chat?.id == chat.id }) {
                nc.popToRootViewController(animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    self?.segmentView?.item = .now
                    self?.showFullScreen()
                    self?.showExperience(experience: experience, scroll: true, showChat: showChat, showCall: showCall)
                }
            } else {
                if let chatVC = nc.topViewController as? ChatViewController,
                    chatVC.chatId == chatId {
                    return
                }
                
                let vc = ChatViewController()
                _ = vc.view
                vc.setup(chat: chat)
                nc.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func highlightRequestsTabIfNeeded() {
        let unread = ChatService.shared.getChats().reduce(0) { (previous, chat) -> Int in
            guard chat.members.count > 1,
                chat.members.first(where: { $0.isMe() }) != nil
                else { return previous }
            let unread = chat.messages?.filter { !$0.isMine() && $0.readAt == nil && $0.type != .here }.count ?? 0
            return previous + unread
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.setRightNavigationButton(unread)
        }
    }
    
    private func presentRequests() {
        let vc = RequestsViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
