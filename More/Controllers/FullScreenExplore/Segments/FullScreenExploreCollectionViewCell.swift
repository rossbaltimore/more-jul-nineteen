//
//  FullScreenExploreCollectionViewCell.swift
//  More
//
//  Created by Luko Gjenero on 10/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExploreCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var content: FullScreenExploreCollectionViewCellContent!
    
    var experience: Experience?
    
    var likeTap: ((_ liked: Bool)->())?
    var creatorTap: (()->())?
    var shareTap: (()->())?
    var dropInTap: (()->())?
    var showProfile: ((_ user: ShortUser)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        content.likeTap = { [weak self] liked in
            self?.likeTap?(liked)
        }
        content.creatorTap = { [weak self] in
            self?.creatorTap?()
        }
        content.shareTap = { [weak self] in
            self?.shareTap?()
        }
        content.dropInTap = { [weak self] in
            self?.dropInTap?()
        }
        content.showProfile = { [weak self] user in
            self?.showProfile?(user)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        enableCardShadow(color: .black, path: UIBezierPath(roundedRect: bounds, cornerRadius: 16).cgPath)
    }
    
    func setup(for experience: Experience) {
        self.experience = experience
        content.setup(for: experience)
    }
    
    func setupLikes(_ liked: Bool) {
        content.setupLikes(liked)
    }
    
    var image: UIImageView! {
        return content.image
    }
    
    var video: MoreVideoView! {
        return content.video
    }
    
    var videoSnapshot: UIImage? {
        return content.video.snapshot()
    }
    
    func setupEmpty() {
        content.setupEmpty()
    }
    
    func animate() {
        content.animate()
    }
    
    func shownOnTop() {
        guard let e = experience else { return }
        guard let updated = ExperienceTrackingService.shared.updatedExperienceData(for: e.id) else { return }
        guard updated.activePost() != nil else { return }
        
        if let chatId = updated.activePost()?.chat?.id,
            let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) {
            if chat.messages?.first(where: { $0.sender.isMe() && $0.type == .here }) == nil {
                guard let me = ProfileService.shared.profile?.shortUser else { return }
                let msgId = "\(me.id.hashValue)-\(Date().hashValue)"
                let here = Message(
                    id: msgId,
                    createdAt: Date(),
                    sender: me,
                    type: .here,
                    text: "\(me.name) is here",
                    deliveredAt: nil,
                    readAt: nil)
                ChatService.shared.sendMessage(chatId: chatId, message: here)
            }
        }

        content.showTimer(experience: updated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        content.reset()
    }
    
    func start() {
        content.start()
    }
    
    func pause() {
        content.pause()
    }
    
    var page: Int {
        get {
            return content.page
        }
        set {
            content.page = newValue
        }
    }
}
