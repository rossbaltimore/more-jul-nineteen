//
//  FullScreenExperienceInputBar.swift
//  More
//
//  Created by Luko Gjenero on 28/05/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExperienceInputBar: LoadableView {

    @IBOutlet private weak var outlineView: UIView!
    @IBOutlet weak var textView: PlaceholderTextView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var cameraButton: UIButton!
    @IBOutlet private weak var locationButton: UIButton!
    @IBOutlet private weak var outlineLeading: NSLayoutConstraint!
    
    var sendTap: ((_ text: String)->())?
    var cameraTap: (()->())?
    var locationTap: (()->())?
    
    override func setupNib() {
        super.setupNib()
        
        outlineView.layer.cornerRadius = 26
        outlineView.layer.borderColor = UIColor.blueyGrey.cgColor
        outlineView.layer.borderWidth = 1
        
        sendButton.layer.cornerRadius = 20
        sendButton.layer.masksToBounds = true
        sendButton.layer.transform = CATransform3DMakeRotation(.pi * -0.5, 0, 0, 1)
        
        textView.placeholderLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        textView.placeholderLabel.textColor = .whiteThree
        textView.placeholderLabel.text = "Messages"
        textView.keyboardAppearance = .dark
    }
    
    @IBAction private func sendTouch() {
        sendTap?(textView.text ?? "")
    }
    
    @IBAction private func cameraTouch() {
        cameraTap?()
    }
    
    @IBAction private func locationTouch() {
        locationTap?()
    }
    
    func showButtons(_ show: Bool) {
        outlineLeading.constant = show ? 72 : 16
        cameraButton.alpha = show ? 1 : 0
        locationButton.alpha = show ? 1 : 0
        
    }

}
