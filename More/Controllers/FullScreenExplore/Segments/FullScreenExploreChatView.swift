//
//  FullScreenExploreChatView.swift
//  More
//
//  Created by Luko Gjenero on 09/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class FullScreenExploreChatView: LoadableView, UIGestureRecognizerDelegate {
    
    @IBOutlet private weak var tableViewContainer: UIView!
    @IBOutlet private weak var tableView: FullScreenExploreChatTableView!
    @IBOutlet weak var suggestions: ChatSuggestionsView!
    @IBOutlet private weak var page: UIPageControl!
    
    private let fade: CAGradientLayer = {
       let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.colors = [
            UIColor.clear.cgColor,
            UIColor.whiteThree.cgColor,
            UIColor.whiteThree.cgColor,
            UIColor.clear.cgColor]
        return gradient
    }()
    
    private let pan = UIPanGestureRecognizer()
    private var chatId: String? = nil
    private(set) var experience: Experience? = nil
    
    var showProfile: ((_ user: ShortUser)->())?
    var expanding: ((_ progress: CGFloat, _ animated: Bool)->())?
    
    override func setupNib() {
        super.setupNib()
        
        page.numberOfPages = 3
        page.currentPage = 1
        page.isUserInteractionEnabled = false
        
        pan.addTarget(self , action: #selector(panAction(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateMessages(_:)), name: ChatService.Notifications.ChatMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(chatUpdate(_:)), name: ChatService.Notifications.ChatChanged, object: nil)
        
        tableView.showProfile = { [weak self] user in
            self?.showProfile?(user)
        }
        
        suggestions.isWhite = false
        suggestions.selected = { [weak self] (text) in
            self?.sendMessage(text: text)
        }
    }
    
    private var layout = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tableViewContainer.layoutIfNeeded()
        setLayout(isExpanded ? 0 : tableView.frame.height * 0.5)
        expanding?(isExpanded ? 1 : 0, false)
        scrollToBottom(animated: false)
    }
    
    // MARK: - setup
    
    func setup(for experience:Experience) {
        self.experience = experience
        
        tableView.chat = nil
        tableView.setup(for: [], forceScrollToBottom: false, animated: false)
        tableView.isScrollEnabled = false
        pan.isEnabled = true
        isExpanded = false
        firstLoad = true
        suggestions.isHidden = true
        setNeedsLayout()
        
        chatId = nil
        if let chat = experience.activePost()?.chat {
            chatId = chat.id
            ChatService.shared.getChat(chatId: chat.id, load: true) { [weak self] (chat) in
                guard let chat = chat else { return }
                guard chat.id == self?.chatId else { return }
                let updated = ChatService.shared.getChats().first(where: { $0.id == chat.id }) ?? chat
                
                self?.tableView.chat = updated
                self?.setup(chat: updated, scrollToBottom: true, animated: false)
                ChatService.shared.addChat(chat: chat)
            }
        }
        
        let chat = ChatService.shared.getChats().first(where: { $0.id == chatId })
        setPages(chat)
    }
    
    func reset() {
//        if let chatId = chatId {
//            ChatService.shared.removeChat(chatId: chatId)
//        }
        chatId = nil
        firstLoad = true
        suggestions.isHidden = true
    }
    
    private func setPages(_ chat: Chat?) {
        if chat?.videoCall != nil {
            page.numberOfPages = 3
        } else {
            page.numberOfPages = 2
        }
    }
    
    // MARK: - fading
    
    private var topOffset: CGFloat = 0
    
    private func setLayout(_ topOffset: CGFloat) {
        var bounds = tableViewContainer.frame
        bounds.origin = .zero
        fade.frame = bounds
        let height = 40 / Double(bounds.height)
        let top = Double(topOffset / bounds.height)
        fade.locations = [NSNumber(value: top), NSNumber(value: top + height), NSNumber(value: 1 - height * 0.6), 1.0]
        tableViewContainer.layer.mask = fade
        page.layer.transform = CATransform3DMakeTranslation(0, topOffset, 0)
        self.topOffset = topOffset
        updateTableViewInsets()
    }
    
    // MARK: - scrolling
    
    func scrollToBottom(animated: Bool) {
        tableView.scrollToBottom(animated: animated)
    }
    
    private func setupScrolling() {
        pan.addTarget(self, action: #selector(panAction(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
    }
    
    private var dragStart: CGPoint = .zero
    
    @objc private func panAction(_ sender: UIPanGestureRecognizer) {
        let point = sender.location(in: tableViewContainer)
        let offset = point.y - dragStart.y
        switch sender.state {
        case .began:
            dragStart = point
        case .changed:
            adjust(to: offset)
        case .cancelled, .ended, .failed:
            settle(to: offset)
        default: ()
        }
    }
    
    private func adjust(to offset: CGFloat) {
        let start = isExpanded ? 0 : tableViewContainer.frame.height * 0.5
        var end = start + offset
        end = max(0, min(tableViewContainer.frame.height * 0.5, end))
        if isExpanded && end > 0 {
            UIResponder.currentFirstResponder()?.resignFirstResponder()
        }
        setLayout(end)
        expanding?(1 - (end / (tableViewContainer.frame.height * 0.5)), false)
    }
    
    private func settle(to offset: CGFloat) {
        let start = isExpanded ? 0 : tableViewContainer.frame.height * 0.5
        let end = start + offset
        if end < tableViewContainer.frame.height * 0.25 {
            expand(true)
        } else {
            collapse(true)
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == pan {
            if shouldScroll() {
                return true
            } else {
                let point = gestureRecognizer.location(in: page)
                return page.point(inside: point, with: nil)
            }
        }
        return true
    }
    
    private func shouldScroll() -> Bool {
        let point = pan.location(in: tableViewContainer)
        let insets = UIEdgeInsets(top: tableViewContainer.frame.height * 0.5, left: 0, bottom: 0, right: 0)
        guard !isExpanded, tableViewContainer.frame.inset(by: insets).contains(point) else { return false }
        
        let translation = pan.translation(in: self)
        guard abs(translation.x) < abs(translation.y) else { return false }

        if translation.y > 0 {
            return false
        }
        
        return true
    }
    
    // MARK: - expand / collapse
    
    private(set) var isExpanded: Bool = false
    
    func expand(_ animated: Bool) {
        isExpanded = true
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.setLayout(0)
                self?.tableView.isScrollEnabled = true
            }
        } else {
            setLayout(0)
            tableView.isScrollEnabled = true
        }
        expanding?(1, animated)
        
    }
    
    func collapse(_ animated: Bool) {
        isExpanded = false
        let height = tableViewContainer.frame.height * 0.5
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.setLayout(height)
                self?.tableView.isScrollEnabled = false
            }
        } else {
            setLayout(height)
            tableView.isScrollEnabled = false
        }
        expanding?(0, animated)
    }
    
    
    // MARK: - message tracking
    
    @objc private func updateMessages(_ notice: Notification) {
        
        DispatchQueue.main.async { [weak self] in
            guard let chat = self?.tableView.chat else { return }
            guard let chatId =  notice.userInfo?["chatId"] as? String, chat.id == chatId else { return }
            guard let updated = ChatService.shared.getChats().first(where: { $0.id == chat.id }) else { return }
            
            self?.setup(chat: updated)
        }
    }
    
    @objc private func chatUpdate(_ notice: Notification) {
        
        DispatchQueue.main.async { [weak self] in
            let noticeChatId = notice.userInfo?["chatId"] as? String ?? (notice.userInfo?["chat"] as? Chat)?.id
            guard let chatId =  noticeChatId, self?.chatId == chatId else { return }
            guard let updated = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            self?.setPages(updated)
        }
    }
    
    private var firstLoad: Bool = true
    
    private func setup(chat: Chat, scrollToBottom: Bool = false, animated: Bool = true) {
        let cells: [ChatCell] = chat.messages?.map { ChatCell(type: .message, message: $0) } ?? []
        tableView.chat = chat
        tableView.setup(for: cells, forceScrollToBottom: scrollToBottom, animated: animated)
        if firstLoad {
            setupSuggestions()
            firstLoad = false
        }
        
        for msg in chat.messages ?? [] {
            if !msg.isMine() && !msg.haveRead() {
                DispatchQueue.global(qos: .default).async {
                    ChatService.shared.setMessageRead(chatId: chat.id, messageId: msg.id)
                }
            }
        }
    }
    
    // MARK: - send message
    
    func sendMessage(text: String) {
        guard let profile = ProfileService.shared.profile else { return }
        
        let messageId = "\(profile.id.hashValue)-\(Date().hashValue)"
        let message = Message(id: messageId, createdAt: Date(), sender: profile.shortUser, type: .text, text: text, deliveredAt: nil, readAt: nil)
        sendMessage(message)
    }
    
    func sendMessage(_ message: Message) {
        guard let chat = tableView.chat else { return }
        guard let post = experience?.activePost() else { return }
        guard let me = ProfileService.shared.profile else { return }
        
        tableView.newMessage(message)
        ExperienceService.shared.extendPost(experienceId: post.experience.id, post: post, complete: nil)
        ChatService.shared.sendMessage(chatId: chat.id, message: message)
        
        if chat.members.first(where: { $0.isMe() }) == nil {
            guard let experience = experience else { return }
            guard let post = experience.activePost(), post.chat?.id == chatId else { return }
            
            let request = ExperienceRequest(
                id: "",
                createdAt: Date(),
                creator: me.shortUser,
                post: post.short(),
                message: nil)
            ExperienceService.shared.createExperienceRequest(for: experience, post: post, request: request, complete: nil)
        }
    }
    
    func showSuggestions(_ show: Bool) {
        if show {
            setupSuggestions()
        } else {
            suggestions.isHidden = true
        }
    }
    
    // MARK: - suggestions
    
    func setupSuggestions() {
        let rows = tableView.rows
        let messages = rows.filter { $0.message?.isContent() == true }
        
        // no mesages yet
        if messages.isEmpty {
            let items = itemsFromConfig(ConfigService.shared.chatSuggestionsEmpty)
            guard !items.isEmpty else {
                suggestions.isHidden = true
                return
            }
            
            suggestions.isHidden = false
            suggestions.items = items
        }
        
        // no messages from me yet
        else if messages.first(where: { $0.message?.isMine() == true }) == nil {
            let items = itemsFromConfig(ConfigService.shared.chatSuggestionsNotEmpty)
            guard !items.isEmpty else {
                suggestions.isHidden = true
                return
            }
            
            suggestions.isHidden = false
            suggestions.items = items
        }
        
        // hide suggestions
        else {
            suggestions.isHidden = true
        }
        
        updateTableViewInsets()
    }
    
    private func itemsFromConfig(_ string: String) -> [String] {
        return string.split(separator: ";").map { String($0) }
    }
    
    // MARK: - insets
    
    private func updateTableViewInsets() {
        tableView.contentInset = UIEdgeInsets(
            top: topOffset,
            left: 0,
            bottom: suggestions.isHidden ? 20 : 75,
            right: 0)
    }
}
