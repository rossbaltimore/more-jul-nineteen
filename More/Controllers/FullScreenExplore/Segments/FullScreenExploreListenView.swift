//
//  FullScreenExploreListenView.swift
//  More
//
//  Created by Luko Gjenero on 10/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class FullScreenExploreListenView: LoadableView {

    @IBOutlet private weak var page: UIPageControl!
    @IBOutlet private weak var audioVisualizer: CircularAudioVisualizer!
    @IBOutlet private weak var speaker: TimeAvatarView!
    @IBOutlet private weak var userListContainer: UIView!
    @IBOutlet private weak var userList: FullScreenExploreListenUserList!
    @IBOutlet private weak var buttonOverlay: UIImageView!
    
    private var chatId: String? = nil
    
    private let fade: CAGradientLayer = {
       let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [
            UIColor.clear.cgColor,
            UIColor.whiteThree.cgColor,
            UIColor.whiteThree.cgColor]
        return gradient
    }()
    
    var showProfile: ((_ user: ShortUser)->())?
    
    override func setupNib() {
        super.setupNib()
        
        page.numberOfPages = 3
        page.currentPage = 2
        page.isUserInteractionEnabled = false
        
        speaker.progress = 0
        speaker.progressBackgroundColor = .clear
        speaker.outlineColor = UIColor(red: 191, green: 195, blue: 202).withAlphaComponent(0.75)
        speaker.ringSize = 1
        speaker.imagePadding = 4
        
        buttonOverlay.layer.cornerRadius = 32.5
        buttonOverlay.layer.masksToBounds = true
        
        setupAudioVisualizer()
        
        userList.selected = { [weak self] user in
            self?.showProfile?(user)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(chatUpdate(_:)), name: ChatService.Notifications.ChatChanged, object: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        fade.frame = userListContainer.bounds
        let width = 15 / Double(userListContainer.frame.width)
        fade.locations = [0.0, NSNumber(value: width), 1.0]
        userListContainer.layer.mask = fade
    }
    
    // MARK: - setup
    
    func setup(for experience:Experience) {
        if let chat = experience.activePost()?.chat {
            chatId = chat.id
            ChatService.shared.getChat(chatId: chat.id, load: true) { [weak self] (chat) in
                guard let chat = chat else { return }
                guard chat.id == self?.chatId else { return }
                
                self?.setup(chat: chat)
            }
        }
    }
    
    // MARK: - audio visualizer
    
    private func setupAudioVisualizer() {
        audioVisualizer.lenght = 18
    }
    
    private weak var audioTimer: Timer?
    
    func audioStart() {
        audioTimer?.invalidate()
        audioTimer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { [weak self] (timer) in
            
            var data: [CGFloat] = []
            for _ in 0..<7 {
                let random = CGFloat(arc4random_uniform(100))
                if random > 30 {
                    data.append((random - 30) / 70)
                } else {
                    data.append(0)
                }
            }
            self?.audioVisualizer.data = data
        })
    }
    
    func audioStop() {
        audioTimer?.invalidate()
        audioVisualizer.data = [0,0,0,0,0,0,0]
        audioVisualizer.startColor = .whiteThree
        audioVisualizer.endColor = .whiteThree
    }
    
    func audioHiglight(higlight: Bool) {
        if higlight {
            buttonOverlay.isHidden = true
            audioVisualizer.startColor = .periwinkle
            audioVisualizer.endColor = .brightSkyBlue
        } else {
            buttonOverlay.isHidden = false
            audioVisualizer.startColor = .whiteThree
            audioVisualizer.endColor = .whiteThree
        }
    }
    
    private var isListening: Bool = false
    
    @IBAction private func buttonDown() {
        guard let chatId = chatId else { return }
        guard let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
        guard chat.videoCall != nil else { return }
        guard chat.videoCall?.members.first(where: { $0.isMe() }) == nil else { return }
        
        
        isListening = true
        audioHiglight(higlight: true)
        VideoCallService.shared.joinCall(chat: chat, audioEnabled: false, videoEnabled: false, isHidden: true) { (success, errorMsg) in
            if !success {
                print(errorMsg ?? "Unable to start call")
            }
        }
    }
    
    @IBAction private func buttonUp() {
        audioHiglight(higlight: false)
        
        guard isListening else { return }
        isListening = false
        VideoCallService.shared.endCall { (success, errorMsg) in
            if !success {
                print(errorMsg ?? "Unable to end call")
            }
        }
    }
    
    // MARK: - message tracking
    
    @objc private func chatUpdate(_ notice: Notification) {
        
        DispatchQueue.main.async { [weak self] in
            let noticeChatId = notice.userInfo?["chatId"] as? String ?? (notice.userInfo?["chat"] as? Chat)?.id
            guard let chatId =  noticeChatId, self?.chatId == chatId else { return }
            guard let updated = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            self?.setup(chat: updated)
        }
    }
    
    private func setup(chat: Chat) {
        guard let videoCall = chat.videoCall else {
            audioVisualizer.isHidden = true
            userList.isHidden = true
            speaker.imageUrl = ""
            speaker.image = nil
            userList.setup(for: [])
            return
        }
        
        audioVisualizer.isHidden = false
        userList.isHidden = false
        
        let count = UInt32(videoCall.members.count)
        
        if count > 0 {
            let idx = arc4random_uniform(count)
            let member = videoCall.members[Int(idx)]
            speaker.imageUrl = member.avatar
            userList.setup(for: videoCall.members)
        } else {
            speaker.imageUrl = ""
            speaker.image = nil
            userList.setup(for: [])
        }
    }
    
}
