//
//  FullScreenExploreEmptyView.swift
//  More
//
//  Created by Luko Gjenero on 07/07/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExploreEmptyView: LoadableView {

    @IBOutlet private weak var animationView: UIView!
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var button: UIButton!

    var tap: (()->())?
    
    override func setupNib() {
        super.setupNib()
        
        let index = Int(arc4random_uniform(2) + 1)
        animationView.addEmptyAnimation(index: index)
    }
    
    @IBAction private func buttonTouch() {
        tap?()
    }
    
    func play() {
        animationView.lottieView?.play()
    }
    
}
