//
//  FullScreenExploreInfoView.swift
//  More
//
//  Created by Luko Gjenero on 02/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

private let threeLines: CGFloat = 156

@IBDesignable
class FullScreenExploreInfoView: LoadableView, UIGestureRecognizerDelegate {
    
    @IBOutlet private weak var page: UIPageControl!
    @IBOutlet weak var scrollviewContainer: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var typeLabel: HorizontalGradientLabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var quoteLabel: UILabel!
    
    private let pan = UIPanGestureRecognizer()
    private var chatId: String? = nil
    
    private let fade: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.colors = [UIColor.clear.cgColor, UIColor.whiteThree.cgColor, UIColor.whiteThree.cgColor, UIColor.clear]
        return gradient
    }()
    
    override func setupNib() {
        super.setupNib()
        
        page.numberOfPages = 3
        page.currentPage = 0
        page.isUserInteractionEnabled = false
        
        typeLabel.enableStrongShadow(color: .black)
        titleLabel.enableStrongShadow(color: .black)
        quoteLabel.enableStrongShadow(color: .black)
        
        NotificationCenter.default.addObserver(self, selector: #selector(chatUpdate(_:)), name: ChatService.Notifications.ChatChanged, object: nil)
        
        setupScrolling()
    }
    
    func setup(for experience: Experience) {
        typeLabel.gradientColors = [experience.type.gradient.0.cgColor, experience.type.gradient.1.cgColor]
        typeLabel.text = experience.type.rawValue.uppercased()
        hasScrolled = false
        chatId = experience.activePost()?.chat?.id
        
        setTitleAndText(title: experience.title, text: experience.text)
        // setTitleAndText(title: "Test UI ...", text: "Long text er iefierfhe werifwe fewrif herw iofheroiu fhe oiufeiwu hieur ieurwf iouerwh fiouerh fiouerh fiouerwhf iouewrh fiouerhf iouerhf iouehrf iouehr foiuehr fiouerh fioueh ioufheriuof heroiuf herioufh eroif hqeoirf qeoiruh oiqeruhf oieqrh foiqeh foiqerhf oiqehr foiherfiouhqeroif hqerwoif hqeroi qeoriuf oiqeruf oiuqerhf oiuqeh fiouqerh w edqwdw owdoiwej dpowqj dopiweqj opiweqj owjeodjqeowj dqwojqwopejwopid jqweopdjqwepo jwe er iefierfhe werifwe fewrif herw iofheroiu fhe oiufeiwu hieur ieurwf iouerwh fiouerh fiouerh fiouerwhf iouewrh fiouerhf iouerhf iouehrf iouehr foiuehr fiouerh fioueh ioufheriuof heroiuf herioufh eroif hqeoirf qeoiruh oiqeruhf oieqrh foiqeh foiqerhf oiqehr foiherfiouhqeroif hqerwoif hqeroi qeoriuf oiqeruf oiuqerhf oiuqeh fiouqerh w edqwdw owdoiwej dpowqj dopiweqj opiweqj owjeodjqeowj dqwojqwopejwopid jqweopdjqwepo jwe er iefierfhe werifwe fewrif herw iofheroiu fhe oiufeiwu hieur ieurwf iouerwh fiouerh fiouerh fiouerwhf iouewrh fiouerhf iouerhf iouehrf iouehr foiuehr fiouerh fioueh ioufheriuof heroiuf herioufh eroif hqeoirf qeoiruh oiqeruhf oieqrh foiqeh foiqerhf oiqehr foiherfiouhqeroif hqerwoif hqeroi qeoriuf oiqeruf oiuqerhf oiuqeh fiouqerh w edqwdw owdoiwej dpowqj dopiweqj opiweqj owjeodjqeowj dqwojqwopejwopid jqweopdjqwepo jwe")
        // setTitleAndText(title: "Test UI ...", text: "Short text ...")
        
        let chat = ChatService.shared.getChats().first(where: { $0.id == chatId })
        setPages(chat)
        
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        scrollviewContainer.layoutIfNeeded()
        fade.frame = scrollviewContainer.bounds
        let height = 60 / Double(scrollviewContainer.frame.height)
        fade.locations = [0.0, NSNumber(value: height), NSNumber(value: 1 - height * 0.5), 1.0]
        scrollviewContainer.layer.mask = fade
        setupInset()
    }
    
    // MARK: - helpers
    
    @objc private func chatUpdate(_ notice: Notification) {
        
        DispatchQueue.main.async { [weak self] in
            let noticeChatId = notice.userInfo?["chatId"] as? String ?? (notice.userInfo?["chat"] as? Chat)?.id
            guard let chatId =  noticeChatId, self?.chatId == chatId else { return }
            guard let updated = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            self?.setPages(updated)
        }
    }
    
    // MARK: - helpers
    
    private func setTitleAndText(title: String?, text: String) {
        if let title = title {
            titleLabel.font = UIFont(name: "Gotham-Black", size: 25)
            titleLabel.text = title
            quoteLabel.text = text
        } else {
            titleLabel.font = UIFont(name: "Avenir-Medium", size: 16)
            titleLabel.text = text
            quoteLabel.text = ""
        }
    }
    
    private func setupInset() {
        let maxHeight = scrollview.frame.height - threeLines
        var padding = maxHeight
        if scrollview.contentSize.height < threeLines {
            padding = scrollview.frame.height - scrollview.contentSize.height
        }
        isUserInteractionEnabled = padding <= maxHeight
        scrollview.contentInset = UIEdgeInsets(top: padding, left: 0, bottom: 0, right: 0)
        if !hasScrolled {
            scrollview.contentOffset = CGPoint(x: 0, y: -padding)
        }
        updatePage()
    }
    
    private func setPages(_ chat: Chat?) {
        if chat?.videoCall != nil {
            page.numberOfPages = 3
        } else {
            page.numberOfPages = 2
        }
    }
    
    // MARK: - scrolling
    
    private var hasScrolled: Bool = false
    
    private func setupScrolling() {
        pan.addTarget(self, action: #selector(panAction(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
    }
    
    private var dragStart: CGPoint = .zero
    
    @objc private func panAction(_ sender: UIPanGestureRecognizer) {
        let point = sender.location(in: self)
        let offset = dragStart.y - point.y
        switch sender.state {
        case .began:
            dragStart = point
        case .changed:
            dragStart = point
            pan(to: offset)
        case .cancelled, .ended, .failed:
            settle(to: offset)
        default: ()
        }
    }
    
    private func pan(to offset: CGFloat) {
        let minY = ceil(-scrollview.frame.height + threeLines)
        let maxY: CGFloat = floor(scrollview.contentSize.height - scrollview.frame.height)
        let offset = CGPoint(x: 0, y: min(maxY, max(minY, scrollview.contentOffset.y + offset)))
        scrollview.contentOffset = offset
        hasScrolled = true
        updatePage()
    }
    
    private func settle(to offset: CGFloat) {
        pan(to: offset)
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == pan {
            return shouldScroll()
        }
        return true
    }
    
    private func shouldScroll() -> Bool {
        let point = pan.location(in: scrollview)
        let insets = UIEdgeInsets(top: -scrollview.contentOffset.y, left: 0, bottom: 0, right: 0)
        guard scrollview.bounds.inset(by: insets).contains(point) else { return false }
        
        let translation = pan.translation(in: self)
        guard abs(translation.x) < abs(translation.y) else { return false }
        
        let minY = ceil(-scrollview.frame.height * 0.5)
        let maxY: CGFloat = floor(scrollview.contentSize.height - scrollview.frame.height)
        if translation.y < 0 && scrollview.contentOffset.y >= maxY {
            return false
        }
        if translation.y > 0 && scrollview.contentOffset.y <= minY {
            return false
        }
        
        return true
    }
    
    private func updatePage() {
        var offset = -scrollview.contentOffset.y
        if offset < 0 {
            offset = 0
        }
        page.layer.transform = CATransform3DMakeTranslation(0, offset, 0)
    }
}
