//
//  FullScreenExploreChatView+Location.swift
//  More
//
//  Created by Luko Gjenero on 24/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

extension FullScreenExploreChatView {

    func selectLocation() {
        
        guard let post = experience?.activePost() else { return }
        guard let chatId = post.chat?.id else { return }
        guard let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
        
        let vc = ChatMeetingSelectorViewController()
        _ = vc.view
        vc.back = { [weak self] in
            self?.ownerViewController?.dismiss(animated: true, completion: nil)
        }
        vc.selected = { [weak self] (location, name, address, type) in
            
            if let location = location {
                // update
                ExperienceService.shared.updateExperiencePostMeetingLocation(experienceId: post.experience.id, postId: post.id, location: location, name: name, address: address, type: type, complete: nil)
                
                // notify in chat
                guard let me = ProfileService.shared.profile?.shortUser else { return }
                let messageId = "\(me.id.hashValue)-\(Date().hashValue)"
                let additional = Message.additionalMeetingData(location: location, name: name ?? "", address: address ?? "")
                let additionalData = Message.additionalData(from: additional)
                let meetMsg = Message(id: messageId, createdAt: Date(), sender: me, type: .meeting, text: "", additionalData: additionalData, deliveredAt: nil, readAt: nil)
                ExperienceService.shared.extendPost(experienceId: post.experience.id, post: post, complete: nil)
                ChatService.shared.sendMessage(chatId: chat.id, message: meetMsg)
            }
            
            self?.ownerViewController?.dismiss(animated: true, completion: nil)
        }
        ownerViewController?.present(vc, animated: true, completion: nil)
        
        vc.setup(post: post, request: nil, location: post.meeting, name: post.meetingName, address: post.meetingAddress, type: post.meetingType)
    }

}
