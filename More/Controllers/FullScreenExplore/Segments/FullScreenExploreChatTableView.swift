//
//  FullScreenExploreChatTableView.swift
//  More
//
//  Created by Luko Gjenero on 02/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

private let requestCell = String(describing: ChatRequestCell.self)
private let incomingCell = String(describing: ChatIncomingCell.self)
private let outgoingCell = String(describing: ChatOutgoingCell.self)
private let responseCell = String(describing: ChatResponseCell.self)
private let typingCell = String(describing: ChatTypingCell.self)
private let dateCell = String(describing: ChatDateCell.self)
private let meetingCell = String(describing: ChatMeetingCell.self)
private let videoCell = String(describing: ChatVideoCell.self)
private let incomingPhotoCell = String(describing: ChatIncomingPhotoCell.self)
private let incomingVideoCell = String(describing: ChatIncomingVideoCell.self)
private let outgoingPhotoCell = String(describing: ChatOutgoingPhotoCell.self)
private let outgoingVideoCell = String(describing: ChatOutgoingVideoCell.self)
private let userInfoCell = String(describing: ChatUserInfoCell.self)
private let infoCell = String(describing: ChatInfoCell.self)

private let incomingInfoCell = String(describing: FullScreenExploreChatAvatarInfoCell.self)
private let emptyCell = String(describing: FullScreenExploreEmptyCell.self)

class FullScreenExploreChatTableView: ChatTableView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setup()
    }
    
    private func setup() {
        
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: requestCell)
        register(UINib(nibName: fullScreenNib(incomingCell), bundle: nil), forCellReuseIdentifier: incomingCell)
        register(UINib(nibName: fullScreenNib(outgoingCell), bundle: nil), forCellReuseIdentifier: outgoingCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: responseCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: typingCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: dateCell)
        register(UINib(nibName: fullScreenNib(meetingCell), bundle: nil), forCellReuseIdentifier: meetingCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: videoCell)
        register(UINib(nibName: fullScreenNib(incomingPhotoCell), bundle: nil), forCellReuseIdentifier: incomingPhotoCell)
        register(UINib(nibName: fullScreenNib(incomingVideoCell), bundle: nil), forCellReuseIdentifier: incomingVideoCell)
        register(UINib(nibName: fullScreenNib(outgoingPhotoCell), bundle: nil), forCellReuseIdentifier: outgoingPhotoCell)
        register(UINib(nibName: fullScreenNib(outgoingVideoCell), bundle: nil), forCellReuseIdentifier: outgoingVideoCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: userInfoCell)
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: infoCell)
        register(UINib(nibName: incomingInfoCell, bundle: nil), forCellReuseIdentifier: incomingInfoCell)
        
        register(UINib(nibName: emptyCell, bundle: nil), forCellReuseIdentifier: "Dummy")
    }
    
    private func fullScreenNib(_ nib: String) -> String {
        return "FullScreenExplore\(nib)"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < rows.count {
            let rowModel = rows[indexPath.row]
            switch rowModel.type {
            case .message:
                if let message = rowModel.message, let chat = chat {
                    switch message.type {
                    case .here:
                        if !message.sender.isMe(),
                            let cell = tableView.dequeueReusableCell(withIdentifier: incomingInfoCell, for: indexPath) as? ChatBaseCell {
                            cell.setup(for: message, in: chat)
                            cell.avatarTap = { [weak self] in
                                self?.showProfile?(message.sender)
                            }
                            return cell
                        }
                    case .startCall, .endCall, .joined:
                        if let cell = tableView.dequeueReusableCell(withIdentifier: incomingInfoCell, for: indexPath) as? ChatBaseCell {
                            cell.setup(for: message, in: chat)
                            cell.avatarTap = { [weak self] in
                                self?.showProfile?(message.sender)
                            }
                            return cell
                        }
                    default: ()
                    }
                }
            default: ()
            }
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
        
    }
}
