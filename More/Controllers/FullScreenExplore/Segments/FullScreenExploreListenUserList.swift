//
//  FullScreenExploreListenUserList.swift
//  More
//
//  Created by Luko Gjenero on 10/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

private let userCell = "UserCell"

@IBDesignable
class FullScreenExploreListenUserList: UICollectionView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    private var users: [ShortUser] = []
    
    convenience init() {
        self.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    var selected: ((_ user: ShortUser)->())?
    
    private func setup() {
        
        contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        register(UserCell.self, forCellWithReuseIdentifier: userCell)
        delegate = self
        dataSource = self
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 15
            layout.minimumInteritemSpacing = 15
        }
    }
    
    private var cellSize: CGSize {
        return CGSize(width: 40, height: frame.height)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = cellSize
            if !itemSize.equalTo(layout.itemSize) {
                layout.itemSize = itemSize
                collectionViewLayout.invalidateLayout()
                reloadData()
            }
        }
    }
    
    func setup(for users: [ShortUser]) {
        self.users = users
        reloadData()
    }

    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item < users.count {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: userCell, for: indexPath) as? UserCell {
                let user = users[indexPath.item]
                cell.setup(for: user)
                return cell
            }
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item < users.count {
            selected?(users[indexPath.item])
        }
    }
    
    // MARK: - user cell
    
    private class UserCell: UICollectionViewCell {
        
        private let avatar: TimeAvatarView = {
            let avatar = TimeAvatarView()
            avatar.translatesAutoresizingMaskIntoConstraints = false
            avatar.clipsToBounds = true
            avatar.backgroundColor = .clear
            avatar.progress = 0
            avatar.progressBackgroundColor = .clear
            avatar.outlineColor = UIColor(red: 191, green: 195, blue: 202).withAlphaComponent(0.75)
            avatar.ringSize = 1
            avatar.imagePadding = 3
            return avatar
        }()
        
        private let name: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "Avenir-Heavy", size: 11)
            label.textColor = .whiteThree
            label.backgroundColor = .clear
            return label
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            layer.masksToBounds = true
            
            contentView.addSubview(avatar)
            contentView.addSubview(name)
            
            avatar.widthAnchor.constraint(equalTo: avatar.heightAnchor).isActive = true
            avatar.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            avatar.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            avatar.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            
            name.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            name.topAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 5).isActive = true
            
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for user: ShortUser) {
            avatar.imageUrl = user.avatar
            name.text = user.name
        }
    }
    
}
