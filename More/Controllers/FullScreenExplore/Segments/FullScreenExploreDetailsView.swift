//
//  FullScreenExploreDetailsView.swift
//  More
//
//  Created by Luko Gjenero on 02/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExploreDetailsView: LoadableView, UIScrollViewDelegate {

    @IBOutlet private weak var info: FullScreenExploreInfoView!
    @IBOutlet private weak var chatView: FullScreenExploreChatView!
    @IBOutlet private weak var listen: FullScreenExploreListenView!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    @IBOutlet private weak var infoTop: NSLayoutConstraint!
    @IBOutlet private var listenTrailing: NSLayoutConstraint!
 
    private var chatId: String? = nil
    
    var blur: ((_ progress: CGFloat, _ animated: Bool)->())?
    var hide: ((_ progress: CGFloat, _ animated: Bool)->())?
    var showProfile: ((_ user: ShortUser)->())?
    
    override func setupNib() {
        super.setupNib()
        
        chatView.expanding = { [weak self] progress, animated in
            if self?.page ==  1 {
                self?.blur?(progress, animated)
            } else {
                self?.blur?(0, false)
            }
        }
        
        chatView.showProfile = { [weak self] user in
            self?.showProfile?(user)
        }
        
        listen.showProfile = { [weak self] user in
            self?.showProfile?(user)
        }

        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(chatUpdate(_:)), name: ChatService.Notifications.ChatChanged, object: nil)
    }
    
    func setup(for experience: Experience) {
        chatId = experience.activePost()?.chat?.id
        info.setup(for: experience)
        chatView.setup(for: experience)
        listen.setup(for: experience)
        
        guard let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
        setPages(chat)
    }
    
    func setupTop(_ top: CGFloat) {
        infoTop.constant = top
        layoutIfNeeded()
    }
    
    func reset() {
        chatView.reset()
    }
    
    // MARK: - scrolling
    
    var page: Int {
        get {
            return Int(round(scrollView.contentOffset.x / scrollView.frame.width))
        }
        set {
            setPage(page: newValue, animated: false)
        }
    }
    
    func setPage(page: Int, animated: Bool) {
        var frame = self.scrollView.bounds
        frame.origin.x = frame.width * CGFloat(page)
        if frame.origin.x < scrollView.contentSize.width - frame.width * 0.5 {
            scrollView.scrollRectToVisible(frame, animated: animated)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var alpha = scrollView.contentOffset.x / scrollView.frame.width
        if alpha > 1 {
            alpha = 2 - alpha
        }
        
        if chatView.isExpanded {
            blur?(alpha, false)
        } else {
            blur?(0, false)
        }
        hide?(alpha, false)
        
        if alpha < 0.5 || alpha > 1.5 {
            UIResponder.resignAnyFirstResponder()
        }
        
        if page >= 2 {
            listen.audioStart()
        } else {
            listen.audioStop()
        }
        
        if alpha == 1 {
            runTutorials()
        }
    }
    
    // MARK: - tutorials
    private func runTutorials() {
        // check the tutorials
        if TutorialService.shared.shouldShow(tutorial: .message) {
            let anchor =  chatView.convert(CGPoint(x: (chatView.bounds.width - 149) / 2, y: chatView.bounds.maxY), to: self)
            TutorialService.shared.show(tutorial: .message, anchor: anchor, container: self, above: true, dark: false)
        }
    }
    
    // MARK: - expand / collapse
    
    func expand(animated: Bool) {
        chatView.expand(animated)
    }
    
    func collapse(animated: Bool) {
        chatView.collapse(animated)
    }
    
    // MARK: - send message
    
    func sendMessage(text: String) {
        chatView.sendMessage(text: text)
    }
    
    func openAlbum() {
        chatView.openAlbum()
    }
    
    func openLocation() {
        chatView.selectLocation()
    }
    
    func showSuggestions(_ show: Bool) {
        chatView.showSuggestions(show)
    }
    
    // MARK: - updates
    
    @objc private func chatUpdate(_ notice: Notification) {
        
        DispatchQueue.main.async { [weak self] in
            let noticeChatId = notice.userInfo?["chatId"] as? String ?? (notice.userInfo?["chat"] as? Chat)?.id
            guard let chatId =  noticeChatId, self?.chatId == chatId else { return }
            guard let updated = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            self?.setPages(updated)
        }
    }
    
    private func setPages(_ chat: Chat?) {
        if chat?.videoCall != nil {
            listen.isHidden = false
            listenTrailing.isActive = true
        } else {
            listen.isHidden = true
            listenTrailing.isActive = false
        }
    }
}
