//
//  FullScreenExploreCollectionViewCellContent.swift
//  More
//
//  Created by Luko Gjenero on 10/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

private let spacing: CGFloat = 10

@IBDesignable
class FullScreenExploreCollectionViewCellContent: LoadableView, UIGestureRecognizerDelegate, UIViewKeyboardDelegate {

    @IBOutlet private weak var view: VerticalGradientView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var video: MoreVideoView!
    @IBOutlet private weak var status: ExperienceStatusView!
    @IBOutlet private weak var blur: UIVisualEffectView!
    @IBOutlet private weak var topGradient: VerticalGradientView!
    @IBOutlet private weak var details: FullScreenExploreDetailsView!
    @IBOutlet private weak var rightGradient: HorizontalGradientView!
    @IBOutlet private weak var bottomGradient: VerticalGradientView!
    @IBOutlet private weak var inputBar: FullScreenExperienceInputBar!
    @IBOutlet private weak var inputBarBottom: NSLayoutConstraint!
    @IBOutlet private weak var inputBarTrailing: NSLayoutConstraint!
    @IBOutlet private weak var dropInButton: UIButton!
    
    // timer
    @IBOutlet private weak var timerContainer: HorizontalGradientView!
    @IBOutlet private weak var timerLabel: UILabel!
    private var timer: Timer?
    private var fadeTimer: Timer?
    
    var likeTap: ((_ liked: Bool)->())?
    var creatorTap: (()->())?
    var shareTap: (()->())?
    var dropInTap: (()->())?
    var showProfile: ((_ user: ShortUser)->())?
    
    override func setupNib() {
        super.setupNib()
        
        view.colors = [
            UIColor(red: 255, green: 255, blue: 255).cgColor,
            UIColor(red: 249, green: 249, blue: 249).cgColor
        ]
        view.locations = [0, 1]
        
        video.contentMode = .scaleAspectFill
        
        timerContainer.layer.masksToBounds = true
        timerContainer.layer.cornerRadius = 15
        timerContainer.colors = [
            UIColor(red: 3, green: 255, blue: 191).cgColor,
            UIColor.brightSkyBlue.cgColor,
            UIColor(red: 48, green: 183, blue: 255).cgColor,
            UIColor.periwinkle.cgColor
        ]
        timerContainer.locations = [0, 0.32, 0.6, 1]
        
        topGradient.colors = [
            UIColor.black.withAlphaComponent(0.35).cgColor,
            UIColor.clear.cgColor
        ]
        
        rightGradient.colors = [
            UIColor.clear.cgColor,
            UIColor.black.withAlphaComponent(0.29).cgColor
        ]
        
        bottomGradient.colors = [
            UIColor.clear.cgColor,
            UIColor.black.withAlphaComponent(0.6).cgColor
        ]
        
        status.share = { [weak self] in
            self?.shareTap?()
        }
        
        status.creator = { [weak self] in
            self?.creatorTap?()
        }
        
        status.like = { [weak self] liked in
            self?.likeTap?(liked)
        }
        
        details.blur = { [weak self] progress, animated in
            self?.setBlur(progress, animated)
        }
        details.hide = { [weak self] progress, animated in
            self?.hide(progress, animated)
        }
        details.showProfile = { [weak self] user in
            self?.showProfile?(user)
        }
        
        inputBar.sendTap = { [weak self] text in
            let trimmed = text.trimmingCharacters(in: .whitespacesAndNewlines)
            guard !trimmed.isEmpty else { return }
            self?.inputBar.textView.text = ""
            self?.details.sendMessage(text: trimmed)
        }
        inputBar.cameraTap = { [weak self] in
            self?.details.openAlbum()
        }
        inputBar.locationTap = { [weak self] in
            self?.details.openLocation()
        }
        
        bottomContraint = inputBarBottom
        trackKeyboard(onlyFor: [inputBar.textView])
        trackKeyboardAndPushUp()
        keyboardDelegate = self
    }
    
    func setup(for experience: Experience, photo: UIImage? = nil) {
        if let photo = photo {
            video.reset()
            video.isHidden = true
            image.isHidden = false
            image.image = photo
        } else {
            if let first = experience.images.first {
                if first.isVideo == true {
                    video.isHidden = false
                    image.isHidden = true
                    if let url = URL(string: first.url) {
                        video.setup(for: url, preview: URL(string: first.previewUrl ?? ""))
                        video.isSilent = true
                        video.play(loop: true)
                    } else {
                        video.reset()
                    }
                } else {
                    video.isHidden = true
                    image.isHidden = false
                    image.sd_progressive_setImage(with: URL(string: experience.images.first?.url ?? ""), placeholderImage: UIImage.signalPlaceholder(), options: SDWebImageOptions.highPriority)
                }
            } else {
                video.isHidden = true
                image.isHidden = false
                image.sd_cancelCurrentImageLoad_progressive()
            }
        }
        
        details.isHidden = false
        status.isHidden = false
        
        details.setup(for: experience)
        details.showSuggestions(false)
        
        status.setup(for: experience)
        
        status.share = { [weak self] in
            self?.shareTap?()
        }
        
        status.like = { [weak self] liked in
            self?.likeTap?(liked)
        }
        
        status.creator = { [weak self] in
            self?.creatorTap?()
        }
        
        setupTimer(for: experience)
    }
    
    func setupEmpty() {
        video.reset()
        video.isHidden = true
        image.isHidden = false
        image.sd_cancelCurrentImageLoad_progressive()
        image.image = nil
        details.isHidden = true
        status.isHidden = true
        timerContainer.isHidden = true
    }
    
    func setupLikes(_ liked: Bool) {
        // TODO
    }
    
    func animate() {
        view.animate(to: [
            UIColor(red: 210, green: 210, blue: 210).cgColor,
            UIColor(red: 240, green: 240, blue: 240).cgColor
        ])
    }
    
    func reset() {
        view.layer.removeAllAnimations()
        timer?.invalidate()
        video.reset()
        details.reset()
    }
    
    func start() {
        if !video.isHidden, !video.isPlaying {
            video.play(loop: true)
        }
    }
    
    func pause() {
        if !video.isHidden {
            video.pause()
        }
    }
    
    func showTimer(experience: Experience) {
        
        return
        
//        let alpha = timerContainer.alpha
//        setupTimer(for: experience)
//        timerContainer.alpha = alpha
//
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            self?.timerContainer.alpha = 1
//        }
//
//        fadeTimer?.invalidate()
//        fadeTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { [weak self] _ in
//            UIView.animate(
//                withDuration: 0.3,
//                animations: {
//                    self?.timerContainer.alpha = 0
//                })
//        })
    }
    
    var page: Int {
        get {
            return details.page
        }
        set {
            details.page = newValue
        }
    }
    
    // MARK: - helpers
    
    private func setupTimer(for experience: Experience) {
        if let post = experience.activePost() {
            timerContainer.isHidden = false
            timerContainer.alpha = 0
            fadeTimer?.invalidate()
            
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] _ in
                
                let left = Int(round(post.expiresAt.timeIntervalSinceNow))
                let hours = left / 3600
                let minutes = (left / 60) % 60
                let seconds = left % 60
                
                if hours > 0 {
                    self?.timerLabel.text = "Time expires in \(hours):\(String(format: "%02d",minutes)):\(String(format: "%02d", seconds))"
                } else {
                    self?.timerLabel.text = "Time expires in \(minutes):\(String(format: "%02d", seconds))"
                }
                
                if left < 1 {
                    self?.timerContainer.isHidden = true
                    self?.timer?.invalidate()
                }
            })
        } else {
            timerContainer.isHidden = true
        }
    }
    
    // MARK: - layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let top = status.frame.maxY - details.frame.minY + 20
        details.setupTop(top)
    }
    
    // MARK: - blur / hide
    
    private func setBlur(_ progress: CGFloat, _ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.blur.alpha = progress
            }
        } else {
            blur.alpha = progress
        }
    }
    
    private func hide(_ progress: CGFloat, _ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.status.alpha = 1 - progress
            }
        } else {
            status.alpha = 1 - progress
        }
    }
    
    // MARK: - keyboard
    
    func updateLayoutForKeyBoardUp() {
        inputBarTrailing.constant = 0
        inputBar.showButtons(true)
        details.setPage(page: 1, animated: false)
        details.showSuggestions(true)
        details.expand(animated: false)
    }
    
    func updateLayoutForKeyBoardDown() {
        details.showSuggestions(false)
        inputBarTrailing.constant = 149
        inputBar.showButtons(false)
    }
    
    // MARK: - drop in
    
    @IBAction private func dropInTouch() {
        dropInTap?()
    }
}
