//
//  FullScreenExploreViewController.swift
//  More
//
//  Created by Luko Gjenero on 28/05/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import FirebaseAuth
import IGListKit
import Firebase

class FullScreenExploreViewController: MoreBaseViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    private static let cellIdentifier = "FullScreenExploreCollectionViewCell"
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet private weak var emptyView: FullScreenExploreEmptyView!
    
    private lazy var adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    private var dataSource: FullScreenExploreListAdapter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = [.all]
        
        adapter.collectionView = collectionView
        
        dataSource = FullScreenExploreListAdapter(adapter: adapter)
        
        dataSource.selected = { [weak self] (experience) in
            self?.itemSelected(experience: experience)
            self?.reportExperienceExpanded(experience)
        }
        
        dataSource.liked = { (experience) in
            ExperienceService.shared.likeExperience(experience: experience, completion: nil)
        }
        
        dataSource.disliked = { (experience) in
            ExperienceService.shared.dislikeExperience(experience: experience, completion: nil)
        }
        
        dataSource.creator = { [weak self] (experience) in
            let root = self?.navigationController ?? self
            root?.presentUser(experience.creator.id)
        }
        
        dataSource.share = { [weak self] (experience) in
            guard let root = self?.navigationController ?? self else { return }
            LinkService.shareExperienceLink(experience: experience, complete: { (url) in
                ShareService.shareSignal(link: url, from: root, complete: { success in
                    if success {
                        ExperienceService.shared.shareExperience(experience: experience, completion: nil)
                    }
                })
            })
        }
        dataSource.showProfile = { [weak self] user in
            let root = self?.navigationController ?? self
            root?.presentUser(user.id)
        }
        
        dataSource.dropIn = { [weak self] (experience) in
            guard let post = experience.activePost() else { return }
            guard let chatId = post.chat?.id else { return }
            guard let chat = ChatService.shared.getChats().first(where: { $0.id == chatId }) else { return }
            
            self?.navigationController?.setNavigationBarHidden(true, animated: true)
            self?.collectionView.isScrollEnabled = false
            self?.joinCall(chat: chat)
            
            guard let me = ProfileService.shared.profile?.shortUser else { return }
            let request = ExperienceRequest(
                id: "",
                createdAt: Date(),
                creator: me,
                post: post.short(),
                message: nil)
            ExperienceService.shared.createExperienceRequest(for: experience, post: post, request: request, complete: nil)
        }
        
        dataSource.reload = { [weak self] in
            self?.updateEmptyView()
            self?.checkCenterCell()
        }
        
        dataSource.scrolled = { [weak self] in
            self?.checkCenterCell()
        }
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }
        
        emptyView.tap = { [weak self] in
            if let explore = self?.parent as? ExploreViewController {
                explore.segmentView?.item = .ideas
                explore.showCardView()
            }
        }
    
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        // setup the deeplinking
        DeepLinkService.shared.rootView = navigationController as? MoreTabBarNestedNavigationController
        
        // mock
        // mockNewSignals()
    }
    
    deinit {
        // release the deeplinking
        DeepLinkService.shared.rootView = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = dataSource.cellSize
            if !itemSize.equalTo(layout.itemSize) {
                layout.itemSize = itemSize
                collectionView.collectionViewLayout.invalidateLayout()
                // collectionView.reloadData()
                adapter.reloadData(completion: nil)
                updateEmptyView()
            }
        }
    }
    
    private var isFirst: Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
//
//            if let ex = self?.dataSource.experiences.first {
//                self?.presentJoinedVirtualBubble(for: ex)
//            }
//
//        }
        
        guard isFirst else { return }
        isFirst = false
        
        updateEmptyView()
        checkCenterCell()
        
        // test()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isVisible()
        
        guard isFirst else { return }
    }
    
    func isVisible() {
        checkCenterCell()
    }
    
    private var settingsShowing: Bool = false
    
    override var prefersStatusBarHidden: Bool {
        return settingsShowing
    }
    
    @objc private func toForeground() {
        adapter.reloadData(completion: nil)
        if emptyView.alpha > 0 {
            emptyView.play()
        }
    }
    
    // MARK: - Scroll
    
    func scrollToTop(animated: Bool) {
        collectionView.scrollRectToVisible(.zero, animated: animated)
    }
    
    // MARK: - Selection
    
    private func itemSelected(experience: Experience) {
        // TODO
    }
    
    private func reportExperienceExpanded(_ experience: Experience) {
        Analytics.logEvent("ExperienceExpanded", parameters: ["experienceId": experience.id, "title": experience.title , "text": experience.text])
    }
    
    // MARK: - pull up to show the signal details
    
    private var currentCell: FullScreenExploreCollectionViewCell? {
        let idx = Int(round((collectionView.contentOffset.x  + collectionView.contentInset.left) / dataSource.cellSize.width))
        return cell(for: idx)
    }
    
    private func cell(for index: Int) -> FullScreenExploreCollectionViewCell? {
        guard index >= 0 && index < dataSource.filteredExperiences.count else { return nil }
        return collectionView.cellForItem(at: IndexPath(item: 0, section: index)) as? FullScreenExploreCollectionViewCell
    }

    // MARK: - empty view
    
    private func updateEmptyView() {
        let hide = dataSource.loading || dataSource.filteredExperiences.count > 0
        UIView.animate(
            withDuration: 0.2,
            animations: { [weak self] in
                self?.emptyView.alpha = hide ? 0 : 1
            },
            completion: { [weak self] (finished) in
                if !hide {
                    self?.emptyView.play()
                }
            })
    }
    
    // MARK: - show signal
    
    func showExperience(experience: Experience, check: Bool = false, scroll: Bool = false, showChat: Bool = false, showCall: Bool = false) {
        dataSource.addExperience(experience, check: check, scroll: scroll, showChat: showChat, showCall: showCall)
    }
    
    // MARK: - show the timer on cards
    
    private func checkCenterCell() {
        guard !dataSource.filteredExperiences.isEmpty else { return }
        guard let cell = currentCell else { return }
        let offset = collectionView.contentOffset.x + collectionView.contentInset.left
        
        // countdown & tutorials
        if offset.truncatingRemainder(dividingBy: dataSource.cellSize.width) == 0 {
            cell.shownOnTop()
            runTutorials(cell: cell)
        }
    }
    
    // MARK: - show profile
    private func showProfile(for experience: Experience) {
        let users = experience.activeGroup()
        let root = navigationController ?? self
        if users.count > 1 {
            root.presentGroup(users)
        } else if let user = users.first {
            root.presentUser(user.id)
        }
    }
    
    private func editOrDelete(_ experience: Experience) {
        let alert = UIAlertController(title: nil, message: "What would you like to do?", preferredStyle: .actionSheet)
        
        let edit = UIAlertAction(title: "Edit", style: .default, handler: { [weak self] _ in
            self?.editExperience(experience)
        })
        
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.deleteExperience(experience)
        })
        
        alert.addAction(edit)
        alert.addAction(delete)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        presentedViewController?.present(alert, animated: true, completion: nil)
    }
    
    private func editExperience(_ experience: Experience) {
        guard let nc = presentedViewController as? SignalDetailsNavigation else { return }
        
        LocationService.shared.requestAlways()
        let vc = CreateSignalViewController()
        vc.cancelTap = { [weak nc] in
            nc?.dismiss(animated: true, completion: nil)
        }
        vc.finished = { [weak self] (experience, claimed) in
            self?.dismiss(animated: true, completion: {
                if claimed {
                    self?.requested(for: experience)
                }
            })
        }
        _ = vc.view
        vc.setup(for: experience)
        nc.present(vc, animated: true, completion: nil)
        vc.presentationController?.delegate = self
    }
    
    private func deleteExperience(_ experience: Experience) {
        let alert = UIAlertController(title: "Delete This Time?", message: "This will delete this Time everywhere in the app and cannot be undone.", preferredStyle: .alert)
        
        let report = UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.executeDeleteExperience(experience)
        })
        
        alert.addAction(report)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        presentedViewController?.present(alert, animated: true, completion: nil)
    }
    
    private func executeDeleteExperience(_ experience: Experience) {
        presentedViewController?.showLoading()
        ExperienceService.shared.deleteExperience(experienceId: experience.id) { [weak self] (success, errorMsg) in
            self?.presentedViewController?.hideLoading()
            if success {
                self?.dismiss(animated: true, completion: {
                    self?.dataSource.removeExperience(id: experience.id)
                })
            }
        }
    }
    
    // MARK: - chat
    
    func isShowingChat(chatId: String) -> Bool {
        if let cell = currentCell,
            let post = cell.experience?.activePost(),
            post.chat?.id == chatId {
            return cell.page == 1
        }
        return false
    }
    
    // MARK: - tutorials
    private func runTutorials(cell: FullScreenExploreCollectionViewCell) {
        guard let parent = navigationController?.topViewController as? ExploreViewController,
            parent.isShowingNow(),
            presentedViewController == nil,
            parent.presentedViewController == nil,
            navigationController?.presentedViewController == nil
            else { return }
        
        // check the tutorials
        if let nc = navigationController {
            if TutorialService.shared.shouldShow(tutorial: .claimed),
                let experience = ExperienceTrackingService.shared.updatedExperienceData(for: cell.experience?.id ?? "xx"),
                experience.activePost()?.creator.isMe() == true,
                cell.page == 0 {
                let anchor =  cell.convert(CGPoint(x: cell.bounds.midX, y: cell.bounds.midY + 120), to: nc.view)
                TutorialService.shared.show(tutorial: .claimed, anchor: anchor, container: nc.view, above: true, dark: false)
            }
        }
    }
}

extension FullScreenExploreViewController: UIAdaptivePresentationControllerDelegate {

    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        if let vc = presentationController.presentedViewController as? CreateSignalViewController {
            vc.save()
        }
    }
}
