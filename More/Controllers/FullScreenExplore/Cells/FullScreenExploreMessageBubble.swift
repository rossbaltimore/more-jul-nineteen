//
//  FullScreenExploreMessageBubble.swift
//  More
//
//  Created by Luko Gjenero on 04/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

private let corner: CGFloat = 16
private let tipSize: CGFloat = 6

class FullScreenExploreMessageBubble: UIView {

    @objc enum ItemType: Int {
        case incoming, outgoing
    }
    
    private let blur: UIVisualEffectView = {
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterialDark))
        blur.translatesAutoresizingMaskIntoConstraints = false
        blur.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        blur.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        blur.setContentHuggingPriority(.defaultLow, for: .vertical)
        blur.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return blur
    }()
    
    private let maskingView: UIView = {
        let mask = UIView()
        mask.backgroundColor = .clear
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        mask.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        mask.setContentHuggingPriority(.defaultLow, for: .vertical)
        mask.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return mask
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.masksToBounds = false
        maskingView.layer.masksToBounds = false
        maskingView.layer.addSublayer(bubble)
        maskingView.layer.addSublayer(tip)

        insertSubview(blur, at: 0)
        addSubview(maskingView)
        
        blur.leadingAnchor.constraint(equalTo: leadingAnchor, constant: -tipSize).isActive = true
        blur.trailingAnchor.constraint(equalTo: trailingAnchor, constant: tipSize).isActive = true
        blur.topAnchor.constraint(equalTo: topAnchor).isActive = true
        blur.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        maskingView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        maskingView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        maskingView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        maskingView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        guard let type = ItemType(rawValue: type) else { return }
        setupForType(type)
    }
    
    @IBInspectable var type: Int = ItemType.incoming.rawValue {
        didSet {
            guard let type = ItemType(rawValue: type) else { return }
            setupForType(type)
            setNeedsLayout()
        }
    }
    
    private func setupForType(_ type: ItemType) {
        switch type {
        case .incoming:
            blur.contentView.backgroundColor = UIColor(red: 240, green: 240, blue: 240).withAlphaComponent(0.38)
        case .outgoing:
            blur.contentView.backgroundColor = UIColor.cornflower.withAlphaComponent(0.75)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        bubble.frame = bounds
        bubble.path = bubblePath().cgPath
        tip.frame = tipFrame()
        tip.path = tipPath().cgPath
        
        self.mask = maskingView
    }
    
    // MARK: - bubble

    private func bubblePath() -> UIBezierPath {
        return UIBezierPath(roundedRect: bounds, cornerRadius: corner)
    }

    private let bubble: CAShapeLayer = {
        let bubble = CAShapeLayer()
        bubble.fillColor = UIColor.black.cgColor
        return bubble
    }()

    // MARK: - tip

    private func tipFrame() -> CGRect {
        var frame = CGRect()
        
        frame.size = CGSize(width: tipSize, height: tipSize * 2)
        switch type {
        case ItemType.outgoing.rawValue:
            frame.origin = CGPoint(x: bounds.maxX - 1, y: bounds.midY - tipSize)
        case ItemType.incoming.rawValue:
            frame.origin = CGPoint(x: -tipSize + 1, y: bounds.midY - tipSize)
        default:
            ()
        }

        return frame
    }

    private func tipPath() -> UIBezierPath {
        let path = UIBezierPath()

        switch type {
        case ItemType.incoming.rawValue:
            path.move(to: CGPoint(x: tipSize, y: 2 * tipSize))
            path.addLine(to: CGPoint(x: tipSize, y: 0))
            path.addLine(to: CGPoint(x: 0, y: tipSize))
        case ItemType.outgoing.rawValue:
            path.move(to: CGPoint(x: 0, y: 2 * tipSize))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: tipSize, y: tipSize))
        default:
            ()
        }
        path.close()

        return path
    }

    private let tip: CAShapeLayer = {
        let tip = CAShapeLayer()
        tip.fillColor = UIColor.black.cgColor
        return tip
    }()

}
