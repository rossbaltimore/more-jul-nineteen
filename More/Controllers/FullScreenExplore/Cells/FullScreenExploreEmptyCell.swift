//
//  FullScreenExploreEmptyCell.swift
//  More
//
//  Created by Luko Gjenero on 04/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExploreEmptyCell: ChatBaseCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
//        backgroundColor = .clear
//        contentView.backgroundColor = .clear
//        contentView.heightAnchor.constraint(equalToConstant: 0).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
