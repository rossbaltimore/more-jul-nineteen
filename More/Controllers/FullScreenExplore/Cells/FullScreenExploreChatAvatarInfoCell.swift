//
//  FullScreenExploreChatAvatarInfoCell.swift
//  More
//
//  Created by Luko Gjenero on 02/07/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class FullScreenExploreChatAvatarInfoCell: ChatBaseCell {

    @IBOutlet private weak var left: AvatarImage!
    @IBOutlet private weak var message: UILabel!
    @IBOutlet private weak var right: AvatarImage!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        left.isUserInteractionEnabled = true
        left.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(avatarTouch)))
    }
    
    override func setup(for message: Message, in chat: Chat) {

        let user = chat.members.first { $0.id == message.sender.id } ?? message.sender
        
        if message.sender.isMe() {
            let me = ProfileService.shared.profile?.shortUser ?? user
            right.isHidden = false
            right.sd_progressive_setImage(with: URL(string: me.avatar), placeholderImage: UIImage.profileThumbPlaceholder())
            left.isHidden = true
            left.sd_cancelCurrentImageLoad()
            self.message.textAlignment = .right
            switch message.type {
            case .here:
                self.message.text = "You are here"
            case .startCall:
                self.message.text = "You started the call"
            case .endCall:
                self.message.text = "You ended the call"
            case .joined:
                self.message.text = "You dropped in"
            default: ()
            }
        } else {
            left.isHidden = false
            left.sd_progressive_setImage(with: URL(string: user.avatar), placeholderImage: UIImage.profileThumbPlaceholder())
            right.isHidden = true
            right.sd_cancelCurrentImageLoad()
            self.message.textAlignment = .left
            if message.type == .joined {
                self.message.text = "\(user.name) dropped in"
            } else {
                self.message.text = message.text
            }
        }
        
        
    }
}
