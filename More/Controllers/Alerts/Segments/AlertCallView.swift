//
//  AlertCallView.swift
//  More
//
//  Created by Luko Gjenero on 29/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit
import SwiftMessages

class AlertCallView: MessageView {

    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet private weak var avatar: AvatarImage!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subtitle: UILabel!
    @IBOutlet private weak var tapButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        blur.layer.masksToBounds = true
        blur.layer.cornerRadius = 10
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.borderWidth = 0.5
        backgroundView.layer.borderColor = UIColor.black.withAlphaComponent(12.0/255.0).cgColor
        backgroundView.enableShadow(color: .black)
        
        // button = tapButton
        tapButton.isUserInteractionEnabled = false
        frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 70)
    }
    
    func setup(title: String, subtitle: String, avatar: String) {
        self.title.text = title
        self.subtitle.text = subtitle
        self.avatar.sd_progressive_setImage(with: URL(string: avatar))
    }

}
