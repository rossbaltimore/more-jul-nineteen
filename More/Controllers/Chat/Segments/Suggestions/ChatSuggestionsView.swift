//
//  ChatSuggestionsView.swift
//  More
//
//  Created by Luko Gjenero on 01/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

private let cellIdentifier = "Cell"

@IBDesignable
class ChatSuggestionsView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var isWhite: Bool = true {
        didSet {
           reloadData()
        }
    }
    
    var items: [String] = [] {
        didSet {
            reloadData()
        }
    }
    private(set) var selectedItem: String? = nil
    
    var selected: ((_ item: String)->())?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        register(Cell.self, forCellWithReuseIdentifier: cellIdentifier)
        dataSource = self
        delegate = self
        contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.sectionInset = .zero
            layout.minimumLineSpacing = 16
            layout.minimumInteritemSpacing = 16
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    func select(item: String) {
        selectedItem = item
        reloadData()
    }
    
    func deselect() {
        self.selectedItem = nil
        reloadData()
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item < items.count,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? Cell {
            let item = items[indexPath.item]
            cell.setup(for: item, selected: item == selectedItem, isWhite: isWhite)
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedItem = items[indexPath.item]
        selected?(items[indexPath.item])
        reloadData()
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let blur: UIVisualEffectView = {
            let blur = UIVisualEffectView(effect: UIBlurEffect(style: .systemMaterialDark))
            blur.translatesAutoresizingMaskIntoConstraints = false
            blur.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
            blur.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
            blur.setContentHuggingPriority(.defaultLow, for: .vertical)
            blur.setContentHuggingPriority(.defaultLow, for: .horizontal)
            return blur
        }()
        
        private let label: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "Avenir-Medium", size: 14)
            label.textColor = .darkGrey
            label.backgroundColor = .clear
            return label
        }()
        
        private (set) var heightConstraint: NSLayoutConstraint!
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.layer.cornerRadius = 17.5
            contentView.layer.masksToBounds = true
            
            contentView.addSubview(blur)
            blur.contentView.addSubview(label)
            
            blur.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            blur.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            blur.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            blur.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            
            label.leadingAnchor.constraint(equalTo: blur.contentView.leadingAnchor, constant: 15).isActive = true
            label.trailingAnchor.constraint(equalTo: blur.contentView.trailingAnchor, constant: -15).isActive = true
            label.centerYAnchor.constraint(equalTo: blur.contentView.centerYAnchor).isActive = true
            
            heightConstraint = contentView.heightAnchor.constraint(equalToConstant: 34)
            heightConstraint.isActive = true
            
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    
        func setup(for item: String, selected: Bool = false, isWhite: Bool = true) {
            contentView.isHidden = item.isEmpty
            label.text = item
            if isWhite {
                blur.effect = UIBlurEffect(style: .systemThinMaterialLight)
                blur.contentView.backgroundColor = UIColor.whiteThree.withAlphaComponent(0.9)
                label.textColor = .darkGrey
                enableShadow(color: .black)
            } else {
                blur.effect = UIBlurEffect(style: .systemMaterialDark)
                blur.contentView.backgroundColor = .clear
                label.textColor = .whiteThree
                disableShadow()
            }
        }
    }

}
