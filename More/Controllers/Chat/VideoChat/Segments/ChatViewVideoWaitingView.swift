//
//  ChatViewVideoWaitingView.swift
//  More
//
//  Created by Luko Gjenero on 01/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

class ChatViewVideoWaitingView: LoadableView {
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subtitle: UILabel!
    
    override func setupNib() {
        super.setupNib()
     
        let config = ConfigService.shared.videoCallWaiting
        guard !config.isEmpty else { return }
        
        
        if let data = config.data(using: .utf8),
            let json = (try? JSONSerialization.jsonObject(with: data, options : .allowFragments)) as? [String: Any] {
            
            if let title = json["title"] as? String {
                self.title.text = title
            }
            if let subtitle = json["subtitle"] as? String {
                self.subtitle.text = subtitle
            }
        }
        
    }
}
