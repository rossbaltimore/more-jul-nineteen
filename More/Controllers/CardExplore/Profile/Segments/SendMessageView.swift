//
//  SendMessageView.swift
//  More
//
//  Created by Luko Gjenero on 27/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class SendMessageView: LoadableView {

    @IBOutlet private weak var blur: UIVisualEffectView!
    @IBOutlet private weak var bubble: UIView!
    @IBOutlet private weak var text: UILabel!
    @IBOutlet private weak var arrow: UIImageView!
    @IBOutlet private weak var button: UIButton!
    
    var tap: (()->())?
    
    override func setupNib() {
        super.setupNib()
        
        blur.layer.masksToBounds = true
        bubble.enableShadow(color: .black)
        arrow.enableCardShadow(color: .black)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        blur.layer.cornerRadius = frame.height * 0.5
        bubble.layer.cornerRadius = frame.height * 0.5
        arrow.layer.cornerRadius = (frame.height - 28) * 0.5
    }
    
    @IBAction private func buttonTouch() {
        tap?()
    }
}
