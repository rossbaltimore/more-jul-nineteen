//
//  VideoCallService+Twilio.swift
//  More
//
//  Created by Luko Gjenero on 17/04/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import TwilioVideo
import CallKit

class VideoCallService: NSObject, RoomDelegate, RemoteParticipantDelegate, CameraSourceDelegate {
    
    struct Notifications {
        static let StartCall = NSNotification.Name(rawValue: "com.more.video.start")
        static let JoinCall = NSNotification.Name(rawValue: "com.more.video.join")
        static let LeaveCall = NSNotification.Name(rawValue: "com.more.video.leave")
        static let RejectCall = NSNotification.Name(rawValue: "com.more.video.reject")
        static let AcceptCall = NSNotification.Name(rawValue: "com.more.video.accept")
        
        static let VideoSettingsChanged = NSNotification.Name(rawValue: "com.more.video.videosettings")
        static let AudioSettingsChanged = NSNotification.Name(rawValue: "com.more.video.audiosettings")
        static let CameraChanged = NSNotification.Name(rawValue: "com.more.video.camera")
        static let MembersChanged = NSNotification.Name(rawValue: "com.more.video.members")
        static let CallEnded = NSNotification.Name(rawValue: "com.more.video.end")
    }
    
    static let shared = VideoCallService()
    
    override init() {
        let configuration = CXProviderConfiguration(localizedName: "CallKit More")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportsVideo = true
        configuration.supportedHandleTypes = [.generic]
        if let callKitIcon = UIImage(named: "more-icon") {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }

        callKitProvider = CXProvider(configuration: configuration)
        callKitCallController = CXCallController()
        
        super.init()
        
        callKitProvider.setDelegate(self, queue: nil)
        self.audioDevice.isEnabled = false
//        self.audioDevice.block = {
//            DefaultAudioDevice.DefaultAVAudioSessionConfigurationBlock();
//            let session = AVAudioSession.sharedInstance()
//            do {
//                try session.setCategory(.playAndRecord, mode: .videoChat, options: .allowBluetooth)
//            } catch {
//                print("Audio error: \(error)")
//            }
//        }
        TwilioVideoSDK.audioDevice = self.audioDevice
        
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        toForeground()
    }
    
    // CallKit components
    let callKitProvider: CXProvider
    let callKitCallController: CXCallController
    var startCallCallback: ((_ success: Bool, _ errorMsg: String?)->())?
    var endCallCallback: ((_ success: Bool)->())?
    
    // MARK: - state management
    
    // TODO: - there needs to be a better way to handle this !!!
    private weak var audioTimer: Timer?
    private func startAudioTimer() {
        audioTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] (timer) in
            if self?.chat == nil {
                timer.invalidate()
                return
            }
            if AVAudioSession.sharedInstance().category != .playAndRecord {
                self?.audioDevice.isEnabled = false
                self?.audioDevice.block()
                self?.audioDevice.isEnabled = true
            }
        })
    }
    
    @objc private func toForeground() {
        // nop
    }
    
    @objc private func toBackground() {
        // nop
    }
    
    // MARK: - call data
    
    private var chat: Chat?
    private var uuid: UUID?
    private var enableAudio: Bool = false
    private var enableVideo: Bool = false
    private var isHidden: Bool = false
    private var room: Room?
    private var participants: [RemoteParticipant] = []
    
    private var localAudioTrack: LocalAudioTrack?
    private var localVideoTrack: LocalVideoTrack?
    private var cameraSource: CameraSource?
    private var audioDevice: DefaultAudioDevice = DefaultAudioDevice()
    private var callKitCompletionHandler: ((_ success: Bool)->())?
    
    // MARK: - API
    
    func joinCall(chat: Chat, audioEnabled: Bool = true, videoEnabled: Bool = false, isHidden: Bool = false, complete: ((_ success: Bool, _ errorMsg: String?)->())?) {
        
        let uuid = UUID()
        self.uuid = uuid
        self.chat = chat
        self.enableAudio = audioEnabled
        self.enableVideo = videoEnabled
        self.isHidden = isHidden
        
        performStartCallAction(uuid: uuid, roomName: chat.title,  complete: complete)
    }
    
    private func internalJoinCall() {
        
        guard let chat = chat else { return }
        guard let accessToken = TwilioService.shared.token else { return }
        
        if enableAudio {
            localAudioTrack = LocalAudioTrack()
        }
        
        if enableVideo {
            createVideoSource()
        }
        
        let guid = chat.videoCall?.id ?? chat.id
        
        let connectOptions = ConnectOptions(token: accessToken) { [weak self] (builder) in
            builder.region = "us1"
            builder.roomName = guid
            builder.uuid = self?.uuid
            builder.isNetworkQualityEnabled = true

            if let audioTrack = self?.localAudioTrack {
                builder.audioTracks = [ audioTrack ]
            }
            if let videoTrack = self?.localVideoTrack {
                builder.videoTracks = [ videoTrack ]
            }
        }
        
        self.room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        
        startAudioTimer()
    }

    private func dataCleanup() {
        self.chat = nil
        self.room = nil
        self.participants.removeAll()
        
        self.localAudioTrack = nil
        self.localVideoTrack = nil
    }
    
    private func cleanup(_ chat: Chat, _ view: UIView? = nil) {

        room?.disconnect()
        ChatService.shared.removeMeFromVideoCall(chat: chat, complete: { success, isEmpty, _ in
            if success && isEmpty {
                guard let me = ProfileService.shared.profile?.shortUser else { return }
                let msgId = "\(me.id.hashValue)-\(Date().hashValue)"
                let endedCall = Message(
                    id: msgId,
                    createdAt: Date(),
                    sender: me,
                    type: .endCall,
                    text: "\(me.name) ended the call",
                    deliveredAt: nil,
                    readAt: nil)
                ChatService.shared.sendMessage(chatId: chat.id, message: endedCall)
            }
        })
                
        dataCleanup()
        stopVideoCallTimer()
        audioTimer?.invalidate()
        
        UIApplication.shared.isIdleTimerDisabled = false
        
        guard view != nil else { return }
        
        DispatchQueue.main.async {
            if let view = view {
                var toRemove: [UIView] = []
                for view in view.subviews {
                    if String(describing: type(of: view)).starts(with: "Jitsi") {
                        toRemove.append(view)
                    }
                }
                toRemove.forEach { $0.removeFromSuperview() }
            }
        }
    }
    
    func endCall(complete: ((_ success: Bool, _ errorMsg: String?)->())?) {
        guard let chat = chat else { return }
        guard let uuid = uuid else { return }
        
        performEndCallAction(uuid: uuid) { [weak self] (success) in
            self?.cleanup(chat)
        }
    }
    
    var audioEnabled: Bool {
        get {
            return audioTrack != nil
        }
        set {
            if newValue {
                localAudioTrack = LocalAudioTrack()
                if let audioTrack = localAudioTrack {
                    room?.localParticipant?.publishAudioTrack(audioTrack)
                }
            } else {
                if let audioTrack = localAudioTrack {
                    room?.localParticipant?.unpublishAudioTrack(audioTrack)
                }
                localAudioTrack = nil
            }
            NotificationCenter.default.post(name: Notifications.AudioSettingsChanged, object: self)
        }
    }
    
    var videoEnabled: Bool {
        get {
            return videoTrack != nil
        }
        set {
            if newValue {
                createVideoSource()
                if let videoTrack = localVideoTrack {
                    room?.localParticipant?.publishVideoTrack(videoTrack)
                }
                NotificationCenter.default.post(name: Notifications.VideoSettingsChanged, object: self)
            } else {
                if let videoTrack = localVideoTrack {
                    room?.localParticipant?.unpublishVideoTrack(videoTrack)
                }
                destroyVideoSource { [weak self] in
                    NotificationCenter.default.post(name: Notifications.VideoSettingsChanged, object: self)
                }
            }
        }
    }
    
    var audioTrack: LocalAudioTrack? {
        get {
            return localAudioTrack
        }
    }
    
    var videoTrack: LocalVideoTrack? {
        get {
            return localVideoTrack
        }
    }
    
    func getParticipants() -> [RemoteParticipant] {
        return participants
    }
    
    // MARK: - chat keep alive
    
    private var videoCallTimer: Timer?
    
    private func startVideoCallTimer(_ chat: Chat) {
        videoCallTimer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true) { (timer) in
            ChatService.shared.videoCallTick(for: chat)
        }
    }
    
    private func stopVideoCallTimer() {
        videoCallTimer?.invalidate()
        videoCallTimer = nil
    }
    
    // MARK: - RoomDelegate
    
    func roomDidConnect(room: Room) {
        guard let chat = chat else {
            room.disconnect()
            return
        }
        
        self.room = room
        
        if !isHidden {
            startVideoCallTimer(chat)
            ChatService.shared.addVideoCall(to: chat, id: room.name, sessionId: "") { [weak self] (success, erroMsg) in
                if !success {
                    self?.endCall(complete: nil)
                }
            }
        }
        
        for participant in room.remoteParticipants {
            participant.delegate = self
            participants.append(participant)
        }
        
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self)
        
        if let audioTrack = localAudioTrack {
            room.localParticipant?.publishAudioTrack(audioTrack)
        }
        if let videoTrack = localVideoTrack {
            room.localParticipant?.publishVideoTrack(videoTrack)
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        callKitCompletionHandler?(true)
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        dataCleanup()
        callKitCompletionHandler?(false)
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        endCall(complete: nil)
        callKitCompletionHandler = nil
        NotificationCenter.default.post(name: Notifications.CallEnded, object: self)
    }
    
    func roomDidReconnect(room: Room) {
        
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        
    }
    
    func roomDidStartRecording(room: Room) {
        
    }
    
    func roomDidStopRecording(room: Room) {
        
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self, userInfo: ["userId": participant.identity])
        participant.delegate = self
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        participants.removeAll(where: { $0.identity == participant.identity })
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self, userInfo: ["userId": participant.identity])
    }
    
    func dominantSpeakerDidChange(room: Room, participant: RemoteParticipant?) {
        
    }
    
    // MARK: - RemoteParticipantDelegate
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        /*
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self)
        */
    }
    
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self, userInfo: ["userId": participant.identity])
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        /*
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self)
        */
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self, userInfo: ["userId": participant.identity])
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        /*
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self)
        */
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        print("Twilio Audio Error \(error)")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        participants.removeAll(where: { $0.identity == participant.identity })
        participants.append(participant)
        NotificationCenter.default.post(name: Notifications.MembersChanged, object: self)
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        print("Twilio Video Error \(error)")
    }
    
    // MARK: - CameraSourceDelegate

    func cameraSourceInterruptionEnded(source: CameraSource) {
        print("cameraSourceInterruptionEnded: \(cameraSource.debugDescription)")
        if let videoTrack = localVideoTrack {
            room?.localParticipant?.publishVideoTrack(videoTrack)
        }
    }
    
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        print("cameraSourceDidFail: \(error.localizedDescription)")
    }
    
    func cameraSourceWasInterrupted(source: CameraSource, reason: AVCaptureSession.InterruptionReason) {
        print("cameraSourceWasInterrupted: \(reason) --- \(cameraSource.debugDescription)")
        if let videoTrack = localVideoTrack {
            room?.localParticipant?.unpublishVideoTrack(videoTrack)
        }
    }
    
    // MARK: - camea management
    
    private func createVideoSource() {
        if let camera = CameraSource(delegate: self) {
            localVideoTrack = LocalVideoTrack(source: camera, enabled: true, name: "camera")
            startCameraSource(camera)
            cameraSource = camera
        }
    }
    
    private func destroyVideoSource(_ complete: (()->())?) {
        cameraSource?.stopCapture() { [weak self] _ in
            self?.localVideoTrack = nil
            self?.cameraSource = nil
            complete?()
        }
    }
    
    private func startCameraSource(_ cameraSource: CameraSource) {
        guard let captureDevice = CameraSource.captureDevice(position: .front) else { return }

        let targetSize: CMVideoDimensions
        let cropRatio: CGFloat
        let frameRate: UInt
        
        targetSize = CMVideoDimensions(width: 1024, height: 768)
        cropRatio = CGFloat(targetSize.width) / CGFloat(targetSize.height)
        frameRate = 24

        let preferredFormat = selectVideoFormatBySize(captureDevice: captureDevice, targetSize: targetSize)
        preferredFormat.frameRate = min(preferredFormat.frameRate, frameRate)
        
        let cropDimensions: CMVideoDimensions
        
        if preferredFormat.dimensions.width > preferredFormat.dimensions.height {
            cropDimensions = CMVideoDimensions(
                width: Int32(CGFloat(preferredFormat.dimensions.height) * cropRatio),
                height: preferredFormat.dimensions.height
            )
        } else {
            cropDimensions = CMVideoDimensions(
                width: preferredFormat.dimensions.width,
                height: Int32(CGFloat(preferredFormat.dimensions.width) * cropRatio)
            )
        }
        
        let outputFormat = VideoFormat()
        outputFormat.dimensions = cropDimensions
        outputFormat.pixelFormat = preferredFormat.pixelFormat
        outputFormat.frameRate = 0
        
        cameraSource.requestOutputFormat(outputFormat)
        
        cameraSource.startCapture(device: captureDevice, format: preferredFormat) { [weak self] _, _, error in
            guard error == nil else { return }
            
            // TODO
        }
    }
    
    private func selectVideoFormatBySize(captureDevice: AVCaptureDevice, targetSize: CMVideoDimensions) -> VideoFormat {
        let supportedFormats = Array(CameraSource.supportedFormats(captureDevice: captureDevice)) as! [VideoFormat]
        
        // Cropping might be used if there is not an exact match
        for format in supportedFormats {
            guard
                format.pixelFormat == .formatYUV420BiPlanarFullRange &&
                    format.dimensions.width >= targetSize.width &&
                    format.dimensions.height >= targetSize.height
                else {
                    continue
            }
            
            return format
        }
        
        fatalError()
    }
    
    func flipCamera() {
        guard
            let cameraSource = cameraSource,
            let position = cameraSource.device?.position,
            let captureDevice = CameraSource.captureDevice(position: position == .front ? .back : .front)
            else {
                return
        }
        
        cameraSource.selectCaptureDevice(captureDevice) { [weak self] _, _, error in
            guard error == nil else { return }
            NotificationCenter.default.post(name: Notifications.CameraChanged, object: self)
        }
    }
    
}

// MARK: - CallKit
extension VideoCallService : CXProviderDelegate {

    private func logMessage(messageText: String) {
        print(messageText)
    }
    
    func providerDidReset(_ provider: CXProvider) {
        logMessage(messageText: "providerDidReset:")

        // AudioDevice is enabled by default
        self.audioDevice.isEnabled = true
        
        // end the call
        endCall(complete: nil)
    }

    func providerDidBegin(_ provider: CXProvider) {
        logMessage(messageText: "providerDidBegin")
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didActivateAudioSession:")

        self.audioDevice.isEnabled = true
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didDeactivateAudioSession:")
    }

    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        logMessage(messageText: "provider:timedOutPerformingAction:")
    }

    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        logMessage(messageText: "provider:performStartCallAction:")
        
        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false

        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()

        // handle
        callKitCompletionHandler = { [weak self] success in
            if success {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                    self?.callKitProvider.reportOutgoingCall(with: action.callUUID, connectedAt: nil)
                }
            } else {
                action.fail()
            }
            self?.startCallCallback?(success, "Unknown error")
        }
        
        // start
        internalJoinCall()
        
        // start the call
        action.fulfill()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.callKitProvider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        }
    }
    

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        logMessage(messageText: "provider:performAnswerCallAction:")

        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */

        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false;

        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()
        
        // not supported

        action.fulfill(withDateConnected: Date())
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("provider:performEndCallAction:")

        if let endCallCallback = endCallCallback {
            endCallCallback(true)
        } else {
            if let chat = chat {
                cleanup(chat)
            }
        }
        endCallCallback = nil
        
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        NSLog("provier:performSetMutedCallAction:")
        
        audioEnabled = !action.isMuted
        
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        NSLog("provier:performSetHeldCallAction:")

        let cxObserver = callKitCallController.callObserver
        let calls = cxObserver.calls

        guard calls.first(where:{$0.uuid == action.callUUID}) != nil else {
            action.fail()
            return
        }
        
        // not supported
        
        action.fulfill()
    }
}

// MARK:- Call Kit Actions
extension VideoCallService {

    func performStartCallAction(uuid: UUID, roomName: String?, complete: ((_ success: Bool, _ errorMsg: String?)->())?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = false
        
        let transaction = CXTransaction(action: startCallAction)
        
        startCallCallback = complete
        
        callKitCallController.request(transaction)  { error in
            if let error = error {
                complete?(false, error.localizedDescription)
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
        }
    }

    func performEndCallAction(uuid: UUID, complete: ((_ success: Bool)->())?) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)

        endCallCallback = complete
        
        callKitCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            NSLog("EndCallAction transaction request successful")
        }
    }
}
