//
//  CleanupService.swift
//  More
//
//  Created by Luko Gjenero on 30/07/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import Firebase
import FirebaseFirestore
import FirebaseCrashlytics
import FirebasePerformance
import MapKit
import Geofirestore

class CleanupService {
    
    static let shared = CleanupService()
    
    func cleanupExperienceGeoTags() {
        Firestore.firestore().collection("experienceLocations")
            .getDocuments(completion: { [weak self] (snapshot, error) in
                if let snapshot = snapshot {
                    snapshot.documents.forEach { doc in
                        self?.deleteExperienceGeoTagsIfNecessary(experienceId: doc.documentID)
                    }
                }
            })
    }
    
    func deleteExperienceGeoTagsIfNecessary(experienceId: String) {
        
        func deleteGeoTag(for experienceId: String) {
            Firestore.firestore().document("experienceLocations/\(experienceId)").delete()
            print("[Cleanup Service] Deleting experience GeoTag for ID: \(experienceId)")
        }
        
        Firestore.firestore().document(ExperienceService.Paths.experience(experienceId))
            .getDocument { (snapshot, error) in
                guard error == nil else { return }
                guard let snapshot = snapshot else { return }
                
                if !snapshot.exists {
                    deleteGeoTag(for: experienceId)
                    return
                }
                
                guard let experience = Experience.fromSnapshot(snapshot) else { return }
                
                if experience.schedule?.isDateInside(Date()) == false {
                    deleteGeoTag(for: experienceId)
                }
            }
    }

}
