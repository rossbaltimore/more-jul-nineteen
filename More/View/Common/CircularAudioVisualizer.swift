//
//  CircularAudioVisualizer.swift
//  More
//
//  Created by Luko Gjenero on 15/06/2020.
//  Copyright © 2020 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class CircularAudioVisualizer: UIView {

    @IBInspectable
    var thickness: CGFloat {
        get {
            return maskingLayer.thickness
        }
        set {
            maskingLayer.thickness = newValue
        }
    }
    
    @IBInspectable
    var spacing: CGFloat {
        get {
            return maskingLayer.spacing
        }
        set {
            maskingLayer.spacing = newValue
        }
    }
    
    @IBInspectable
    var lenght: CGFloat {
        get {
            return maskingLayer.lenght
        }
        set {
            maskingLayer.lenght = newValue
        }
    }
    
    var data: [CGFloat] {
        get {
            return maskingLayer.data
        }
        set {
            maskingLayer.data = newValue
        }
    }
    
    @IBInspectable
    var startColor: UIColor = .whiteThree {
        didSet{
            gradient.colors = [
                startColor.cgColor,
                endColor.cgColor
            ]
        }
    }
    
    @IBInspectable
    var endColor: UIColor = .whiteThree {
        didSet{
            gradient.colors = [
                startColor.cgColor,
                endColor.cgColor,
            ]
        }
    }
    
    @IBInspectable
    var startPoint: CGPoint {
        get {
            return gradient.startPoint
        }
        set {
            gradient.startPoint = newValue
        }
    }
    
    @IBInspectable
    var endPoint: CGPoint {
        get {
            return gradient.endPoint
        }
        set {
            gradient.endPoint = newValue
        }
    }
    
    private let maskingLayer:CircularAudioVisualizerLayer = {
        let layer = CircularAudioVisualizerLayer()
        layer.contentsScale = UIScreen.main.scale
        layer.thickness = 3
        layer.spacing = 3
        layer.lenght = 30
        return layer
    }()
    
    private let gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [UIColor.whiteTwo.cgColor,
                        UIColor.whiteTwo.cgColor]
        layer.locations = [0, 1.0]
        layer.startPoint = .zero
        layer.endPoint = CGPoint(x: 1, y: 1)
        return layer
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
        maskingLayer.frame = bounds
    }
    
    private func setup() {
        layer.addSublayer(gradient)
        layer.mask = maskingLayer
    }
}

@IBDesignable
class CircularAudioVisualizerLayer: CALayer {
    
    @NSManaged var thickness: CGFloat
    
    @NSManaged var spacing: CGFloat
    
    @NSManaged var lenght: CGFloat
    
    var data: [CGFloat] = [0,0,0,0,0,0,0,0] {
        didSet {
            recalculateTickData()
        }
    }
    
    @NSManaged private var tickData: [CGFloat]
    
    override func layoutSublayers() {
        super.layoutSublayers()
        recalculateTickData()
    }
    
    override class func needsDisplay(forKey key: String) -> Bool {
        if key == #keyPath(tickData) {
            return true
        }
        if key == #keyPath(thickness) {
            return true
        }
        if key == #keyPath(spacing) {
            return true
        }
        if key == #keyPath(lenght) {
            return true
        }
        return super.needsDisplay(forKey: key)
    }
    
    override func action(forKey key: String) -> CAAction?  {
        if key == #keyPath(tickData) {
            let animation = CABasicAnimation(keyPath: key)
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            animation.fromValue = self.presentation()?.tickData
            return animation
        }
        return super.action(forKey: key)
    }
    
    override func draw(in ctx: CGContext) {
        // draw the lines
        drawTicks(in: ctx)
    }
    
    private func drawTicks(in ctx: CGContext) {
        guard !tickData.isEmpty else { return }
        
        ctx.setStrokeColor(UIColor.whiteThree.cgColor)
        ctx.setLineCap(.round)
        ctx.setLineWidth(thickness)
        
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(center.x, center.y) - lenght

        let numOfTicks = tickData.count
        let anglePart = 2 * CGFloat.pi / CGFloat(numOfTicks)
        
        for i in 0..<numOfTicks {
            let angle = anglePart * CGFloat(i)
            let start = CGPoint(
                x: center.x + radius * cos(angle),
                y: center.y + radius * sin(angle))
            let end = CGPoint(
                x: center.x + (radius + tickData[i]) * cos(angle),
                y: center.y + (radius + tickData[i]) * sin(angle))
        
            ctx.move(to: start)
            ctx.addLine(to: end)
        }
        
        ctx.strokePath()
    }
    
    private func recalculateTickData() {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(center.x, center.y) - lenght
        
        guard radius > 0 else {
            tickData = []
            return
        }
        
        let circumference = 2 * CGFloat.pi * radius
        
        let numOfTicks = Int(circumference / (thickness + spacing))
        let step = Double(data.count) / Double(numOfTicks)
        
        var tickData: [CGFloat] = []
        for i in 0..<numOfTicks {
            
            let idx = Double(i) * step
            
            let start = Int(idx)
            let end = (start + 1) % data.count
            let progress = idx.truncatingRemainder(dividingBy: 1)
            
            let value = Easing.quadraticOut.interpolate(
                time: progress,
                beginning: Double(data[start]),
                change: Double(data[end] - data[start]),
                duration: 1)
            
            tickData.append(CGFloat(max(value, 0)) * lenght)
        }
        
        self.tickData = tickData
    }
}
